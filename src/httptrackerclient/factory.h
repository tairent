/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _httptrackerclient_factory_h
#define _httptrackerclient_factory_h

#include "main/trackerclientfactory.h"

namespace Tairent
{

namespace TrackerClient
{

/** \brief Factory for creating tracker clients for http protocol.
 */
class HTTPTrackerClientFactory : public Tairent::Main::TrackerClientFactory
{
	public:
		/** Creates a factory.
		 */
		HTTPTrackerClientFactory();

		/** Destroys the factory.
		 */
		virtual ~HTTPTrackerClientFactory();

		/** Creates new client.
		 */
		virtual Tairent::Main::TrackerClient *createTrackerClient(Tairent::Main::TorrentClient *client);
};

}; // namespace TrackerClient

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
