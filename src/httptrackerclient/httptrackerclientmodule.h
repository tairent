/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006-2007 David Brodsky                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _httptrackerclient_httptrackerclientmodule_h
#define _httptrackerclient_httptrackerclientmodule_h

#include <tairon/core/module.h>

namespace Tairent
{

namespace TrackerClient
{

class HTTPTrackerClientFactory;

/** \brief Module that provides tracker client factory for http protocol.
 */
class HTTPTrackerClientModule : public Tairon::Core::Module
{
	public:
		/** Creates new factory and registers it.
		 */
		HTTPTrackerClientModule();

		/** Unregisters the factory and destroys it.
		 */
		virtual ~HTTPTrackerClientModule();

	private:
		/** Tracker client factory
		 */
		HTTPTrackerClientFactory *factory;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
