/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/log.h>

#include "factory.h"

#include "main/trackermanager.h"
#include "client.h"

namespace Tairent
{

namespace TrackerClient
{

/* {{{ HTTPTrackerClientFactory::HTTPTrackerClientFactory() */
HTTPTrackerClientFactory::HTTPTrackerClientFactory() : Tairent::Main::TrackerClientFactory()
{
	Tairent::Main::TrackerManager::self()->registerFactory("http", this);
}
/* }}} */

/* {{{ HTTPTrackerClientFactory::~HTTPTrackerClientFactory() */
HTTPTrackerClientFactory::~HTTPTrackerClientFactory()
{
	Tairent::Main::TrackerManager::self()->unregisterFactory("http");
}
/* }}} */

/* {{{ HTTPTrackerClientFactory::createTrackerClient(Tairent::Main::TorrentClient *) */
Tairent::Main::TrackerClient *HTTPTrackerClientFactory::createTrackerClient(Tairent::Main::TorrentClient *client)
{
	DEBUG("Request for tracker for http protocol");
	return new HTTPTrackerClient(client);
}
/* }}} */

}; // namespace Tairent

}; // namespace TrackerClient

// vim: ai sw=4 ts=4 noet fdm=marker
