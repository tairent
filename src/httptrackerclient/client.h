/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _httptracker_client_client_h
#define _httptracker_client_client_h

#include "main/trackerclient.h"

namespace Tairon
{

namespace Net
{

class HTTPClient;
class Timer;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace TrackerClient
{

/** \brief Tracker client for http protocol.
 */
class HTTPTrackerClient : public Tairent::Main::TrackerClient
{
	public:
		/** Creates a tracker client object.
		 *
		 * \param c Torrent that uses this tracker.
		 */
		HTTPTrackerClient(Tairent::Main::TorrentClient *c);

		/** Destroys the object.
		 */
		virtual ~HTTPTrackerClient();

		/** Gets peers from the tracker and passes it to the client.
		 */
		virtual void getPeers();

		/** Sends to the tracker that this client is no longer available.
		 */
		virtual void stop();

	private:
		/** Called when there are data available from the tracker.
		 */
		void dataRead(Tairon::Net::HTTPClient *);

		/** Called when an error occurs.
		 */
		void error(Tairon::Net::HTTPClient *, const char *msg, int err);

		/** Called when an error occurs during stopping.
		 */
		void errorWhileStopping(Tairon::Net::HTTPClient *, const char *, int);

		/** Called when the client is stopped.
		 */
		void stopped(Tairon::Net::HTTPClient *);

		/** Called when an interval timeout occurs.
		 */
		void timeout();

	private:
		/** HTTP client used to get data from the tracker.
		 */
		Tairon::Net::HTTPClient *httpclient;

		/** Timer used for rerequests.
		 */
		Tairon::Net::Timer *timer;

		/** URI of the tracker.
		 */
		String uri;
};

}; // namespace TrackerClient

}; // namespace HTTPTrackerClienti

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
