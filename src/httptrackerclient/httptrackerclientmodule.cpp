/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include "httptrackerclientmodule.h"

#include "factory.h"

namespace Tairent
{

namespace TrackerClient
{

/* {{{ HTTPTrackerClientModule::HTTPTrackerClientModule() */
HTTPTrackerClientModule::HTTPTrackerClientModule() : Tairon::Core::Module()
{
	factory = new HTTPTrackerClientFactory();
}
/* }}} */

/* {{{ HTTPTrackerClientModule::~HTTPTrackerClientModule() */
HTTPTrackerClientModule::~HTTPTrackerClientModule()
{
	delete factory;
}
/* }}} */

}; // namespace TrackerClient

}; // namespace Tairent

EXPORT_MODULE(httptrackerclient, Tairent::TrackerClient::HTTPTrackerClientModule)

// vim: ai sw=4 ts=4 noet fdm=marker
