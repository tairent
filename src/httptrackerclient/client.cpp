/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <sstream>

#include <tairon/core/config.h>
#include <tairon/core/log.h>
#include <tairon/net/httpclient.h>
#include <tairon/net/timer.h>
#include <tairon/util/regexp.h>

#include "client.h"

#include "core/bencode.h"
#include "main/mainthread.h"
#include "main/torrentclient.h"
#include "main/torrentmanager.h"

namespace Tairent
{

namespace TrackerClient
{

/* {{{ HTTPTrackerClient::HTTPTrackerClient(Tairent::Main::TorrentClient *) */
HTTPTrackerClient::HTTPTrackerClient(Tairent::Main::TorrentClient *c) : Tairent::Main::TrackerClient(c)
{
	timer = new Tairon::Net::Timer();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &HTTPTrackerClient::timeout));

	Tairon::Util::RegExp r("http://([[:alnum:]\\.-]+)(:([0-9]+))?(.*)");
	r.match(client->getMetaInfo()["announce"].asString());
	String port(r.matched(3));
	uri = r.matched(4);

	httpclient = new Tairon::Net::HTTPClient(r.matched(1), port.length() ? atoi(port) : 80);
	httpclient->dataReadSignal.connect(Tairon::Core::methodFunctor(this, &HTTPTrackerClient::dataRead));
	httpclient->errorSignal.connect(Tairon::Core::methodFunctor(this, &HTTPTrackerClient::error));
	httpclient->setParameter("info_hash", client->getInfoHash());
	httpclient->setParameter("peer_id", Tairent::Main::TorrentManager::self()->getClientID());
	httpclient->setParameter("port", (*Tairon::Core::Config::self())["torrent-server-port"]);
}
/* }}} */

/* {{{ HTTPTrackerClient::~HTTPTrackerClient() */
HTTPTrackerClient::~HTTPTrackerClient()
{
	if (httpclient->isActive())
		httpclient->close();
	delete httpclient;
	timer->destroy();
}
/* }}} */

/* {{{ HTTPTrackerClient::dataRead(Tairon::Net::HTTPClient *) */
void HTTPTrackerClient::dataRead(Tairon::Net::HTTPClient *)
{
	std::stringstream s(httpclient->getData());

	DEBUG((const char *) String("Tracker data: " + httpclient->getData()));

	try {
		Tairent::Core::BEncode b;
		s >> b;

		if (b.asMap().count("failure reason"))
			WARNING((const char *) String("Tracker failure: " + b["failure reason"].asString()));
		else {
			timer->start(b["interval"].asValue() * 1000, true);

			std::list<Tairent::Main::PeerStruct> peers;
			const Tairent::Core::BEncode::List &plist = b["peers"].asList();
			for (Tairent::Core::BEncode::List::const_iterator it = plist.begin(); it != plist.end(); ++it) {
				Tairent::Main::PeerStruct peer;
				peer.id = (*it)["peer id"].asString();
				peer.ip = (*it)["ip"].asString();
				peer.port = (*it)["port"].asValue();
				peers.push_back(peer);
			}
			client->addPeers(peers);
		}
	} catch (const Tairent::Core::BEncodeException &e) {
		WARNING((const char *) String("BEncode exception: " + (String) e));
		timer->start(120000, true); // try again in 2 minutes
	}
}
/* }}} */

/* {{{ HTTPTrackerClient::error(Tairon::Net::HTTPClient *, const char *, int) */
void HTTPTrackerClient::error(Tairon::Net::HTTPClient *, const char *msg, int err)
{
	WARNING((const char *) String(String("HTTP client error: ") + msg + " (" + String::number(err) + ")"));
}
/* }}} */

/* {{{ HTTPTrackerClient::errorWhileStopping(Tairon::Net::HTTPClient *, const char *, int) */
void HTTPTrackerClient::errorWhileStopping(Tairon::Net::HTTPClient *, const char *, int)
{
	Tairent::Main::MainThread::self()->operationDone();
}
/* }}} */

/* {{{ HTTPTrackerClient::getPeers() */
void HTTPTrackerClient::getPeers()
{
	DEBUG("Client requested peers");

	httpclient->setParameter("event", "started");
	httpclient->setParameter("uploaded", String::number(client->getUploadedSize()));
	httpclient->setParameter("downloaded", String::number(client->getDownloadedSize()));
	httpclient->setParameter("left", String::number(client->getRemainingSize()));

	httpclient->get(uri);
}
/* }}} */

/* {{{ HTTPTrackerClient::stop() */
void HTTPTrackerClient::stop()
{
	Tairent::Main::MainThread::self()->operationInProgress();

	if (httpclient->isActive())
		httpclient->close();

	timer->stop();

	httpclient->dataReadSignal.clear();
	httpclient->dataReadSignal.connect(Tairon::Core::methodFunctor(this, &HTTPTrackerClient::stopped));

	httpclient->errorSignal.clear();
	httpclient->errorSignal.connect(Tairon::Core::methodFunctor(this, &HTTPTrackerClient::errorWhileStopping));

	httpclient->setParameter("event", "stopped");
	httpclient->setParameter("uploaded", String::number(client->getUploadedSize()));
	httpclient->setParameter("downloaded", String::number(client->getDownloadedSize()));
	httpclient->setParameter("left", String::number(client->getRemainingSize()));
	httpclient->get(uri);
}
/* }}} */

/* {{{ HTTPTrackerClient::stopped(Tairon::Net::HTTPClient *) */
void HTTPTrackerClient::stopped(Tairon::Net::HTTPClient *)
{
	Tairent::Main::MainThread::self()->operationDone();
}
/* }}} */

/* {{{ HTTPTrackerClient::timeout() */
void HTTPTrackerClient::timeout()
{
	httpclient->delParameter("event");
	httpclient->get(uri);
}
/* }}} */

}; // namespace TrackerClient

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
