/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <sstream>

#include <tairon/crypto/sha1.h>

#include "bencode.h"

namespace Tairent
{

namespace Core
{

/* {{{ BEncode::BEncode() */
BEncode::BEncode() : type(BEncode::NONE)
{
}
/* }}} */

/* {{{ BEncode::BEncode(const int64_t v) */
BEncode::BEncode(const int64_t v) : type(BEncode::VALUE), value(v)
{
}
/* }}} */

/* {{{ BEncode::BEncode(const String &s) */
BEncode::BEncode(const String &s) : type(BEncode::STRING), string(new String(s))
{
}
/* }}} */

/* {{{ BEncode::BEncode(const BEncode &b) */
BEncode::BEncode(const BEncode &b) : type(b.type)
{
	switch (b.type) {
		case BEncode::VALUE:
			value = b.value;
			break;
		case BEncode::STRING:
			string = new String(*(b.string));
			break;
		case BEncode::LIST:
			list = new BEncode::List(*(b.list));
			break;
		case BEncode::MAP:
			map = new BEncode::Map(*(b.map));
			break;
		default:
			break;
	}
}
/* }}} */

/* {{{ BEncode::BEncode(BEncode::Type t) */
BEncode::BEncode(BEncode::Type t) : type(t)
{
	switch (t) {
		case BEncode::VALUE:
			value = 0;
			break;
		case BEncode::STRING:
			string = new String();
			break;
		case BEncode::LIST:
			list = new List();
			break;
		case BEncode::MAP:
			map = new Map();
			break;
		default:
			break;
	}
}
/* }}} */

/* {{{ BEncode::~BEncode() */
BEncode::~BEncode()
{
	clear();
}
/* }}} */

/* {{{ BEncode::asList() */
BEncode::List &BEncode::asList()
{
	if (!isList())
		throw BEncodeException("BEncode is not type list");

	return *list;
}
/* }}} */

/* {{{ BEncode::asList() const */
const BEncode::List &BEncode::asList() const
{
	if (!isList())
		throw BEncodeException("BEncode is not type list");

	return *list;
}
/* }}} */

/* {{{ BEncode::asMap() */
BEncode::Map &BEncode::asMap()
{
	if (!isMap())
		throw BEncodeException("BEncode is not type map");

	return *map;
}
/* }}} */

/* {{{ BEncode::asMap() const */
const BEncode::Map &BEncode::asMap() const
{
	if (!isMap())
		throw BEncodeException("BEncode is not type map");

	return *map;
}
/* }}} */

/* {{{ BEncode::asString() */
String &BEncode::asString()
{
	if (!isString())
		throw BEncodeException("BEncode is not type string");

	return *string;
}
/* }}} */

/* {{{ BEncode::asString() const */
const String &BEncode::asString() const
{
	if (!isString())
		throw BEncodeException("BEncode is not type string");

	return *string;
}
/* }}} */

/* {{{ BEncode::asValue() */
int64_t &BEncode::asValue()
{
	if (!isValue())
		throw BEncodeException("BEncode is not type value");

	return value;
}
/* }}} */

/* {{{ BEncode::asValue() const */
const int64_t &BEncode::asValue() const
{
	if (!isValue())
		throw BEncodeException("BEncode is not type value");

	return value;
}
/* }}} */

/* {{{ BEncode::clear */
void BEncode::clear()
{
	switch (type) {
		case BEncode::STRING:
			delete string;
			break;
		case BEncode::LIST:
			delete list;
			break;
		case BEncode::MAP:
			delete map;
			break;
		default:
			break;
	}

	type = BEncode::NONE;
}
/* }}} */

/* {{{ BEncode::computeSHA1() const */
String BEncode::computeSHA1() const
{
	std::stringstream stream;
	stream << *this;

	if (stream.fail())
		throw BEncodeException("Cannot write BEncode to stream");

	return Tairon::Crypto::SHA1::hash(stream.str());
}
/* }}} */

/* {{{ BEncode::isList() const */
bool BEncode::isList() const
{
	return (type == BEncode::LIST);
}
/* }}} */

/* {{{ BEncode::isMap() const */
bool BEncode::isMap() const
{
	return (type == BEncode::MAP);
}
/* }}} */

/* {{{ BEncode::isString() const */
bool BEncode::isString() const
{
	return (type == BEncode::STRING);
}
/* }}} */

/* {{{ BEncode::isValue() const */
bool BEncode::isValue() const
{
	return (type == BEncode::VALUE);
}
/* }}} */

/* {{{ BEncode::getType() const */
BEncode::Type BEncode::getType() const
{
	return type;
}
/* }}} */

/* {{{ BEncode::operator=(const BEncode &b) */
BEncode &BEncode::operator=(const BEncode &b)
{
	clear();

	type = b.type;
	switch (type) {
		case BEncode::VALUE:
			value = b.value;
			break;
		case BEncode::STRING:
			string = new String(*(b.string));
			break;
		case BEncode::LIST:
			list = new BEncode::List(*(b.list));
			break;
		case BEncode::MAP:
			map = new BEncode::Map(*(b.map));
			break;
		default:
			break;
	}

	return *this;
}
/* }}} */

/* {{{ BEncode::operator[](const String &key) */
BEncode &BEncode::operator[](const String &key)
{
	if (type != BEncode::MAP)
		throw BEncodeException("Wrong type for BEncode operator [" + key + "]");

	return (*map)[key];
}
/* }}} */

/* {{{ BEncode::operator[](const String &key) const */
const BEncode &BEncode::operator[](const String &key) const
{
	if (type != BEncode::MAP)
		throw BEncodeException("Wrong type for BEncode operator [" + key + "]");

	BEncode::Map::const_iterator it = map->find(key);

	if (it == map->end())
		throw BEncodeException("BEncode operator [] could not find element " + key);

	return it->second;
}
/* }}} */

/* {{{ BEncode::readString(std::istream &s, String &str) */
bool BEncode::readString(std::istream &s, String &str)
{
	int size;
	s >> size;

	if (s.fail() || s.get() != ':')
		return false;

	str.resize(size);

	size_t len = str.length();
	for (size_t i = 0; i < len && s.good(); ++i)
		str[i] = s.get();

	return !s.fail();
}
/* }}} */

/* {{{ operator>>(std::istream &s, BEncode &b) */
std::istream &operator>>(std::istream &s, BEncode &b)
{
	b.clear();

	if (s.peek() < 0) {
		s.setstate(std::istream::failbit);
		return s;
	}

	char c;

	const char *reason = 0;

	switch (c = s.peek()) {
		case 'i':
			s.get();
			s >> b.value;

			if (s.fail() || s.get() != 'e') {
				reason = "Failed to load integer";
				break;
			}

			b.type = BEncode::VALUE;

			return s;

		case 'l':
			s.get();

			b.list = new BEncode::List;
			b.type = BEncode::LIST;

			while (s.good()) {
				if (s.peek() == 'e') {
					s.get();
					return s;
				}

				BEncode::List::iterator it = b.list->insert(b.list->end(), BEncode());

				s >> *it;
			}

			reason = "Failed to load list";

			break;

		case 'd':
			s.get();

			b.map = new BEncode::Map;
			b.type = BEncode::MAP;

			while (s.good()) {
				if (s.peek() == 'e') {
					s.get();
					return s;
				}

				String str;

				if (!BEncode::readString(s, str))
					break;

				s >> (*b.map)[str];
			}

			reason = "Failed to load dictionary";

			break;

		default:
			if (c >= '0' && c <= '9') {
				b.string = new String();
				b.type = BEncode::STRING;

				if (b.readString(s, *b.string))
					return s;
			}

			reason = "Failed to load string";

			break;
	}

	std::stringstream stream;
	stream << reason << " at position " << s.tellg();

	s.setstate(std::istream::failbit);
	b.clear();

	throw BEncodeException(stream.str());

	return s;
}
/* }}} */

/* {{{ operator<<(std::ostream &s, const BEncode &b) */
std::ostream &operator<<(std::ostream &s, const BEncode &b)
{
	switch (b.type) {
		case BEncode::VALUE:
			return s << 'i' << b.value << 'e';
		case BEncode::STRING:
			return s << b.string->size() << ':' << *(b.string);
		case BEncode::LIST:
			s << 'l';

			for (BEncode::List::const_iterator it = b.list->begin(); it != b.list->end(); ++it)
				s << *it;

			return s << 'e';
		case BEncode::MAP:
			s << 'd';

			for (BEncode::Map::const_iterator it = b.map->begin(); it != b.map->end(); ++it)
				s << it->first.size() << ':' << it->first << it->second;

			return s << 'e';
		default:
			break;
	}

	return s;
}
/* }}} */

}; // namespace Core

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
