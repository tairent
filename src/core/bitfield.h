/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _core_bitfield_h
#define _core_bitfield_h

#include <cstddef>

#include <tairon/core/string.h>

namespace Tairent
{

namespace Core
{

/** \brief BitField is an array of bits.
 */
class BitField
{
	public:
		/** Constructs an empty BitField object of specific length.
		 *
		 * \param len Length of the bitfield (i.e. number of bits).
		 */
		BitField(size_t len);

		/** Constructs a BitField object from a memory.
		 */
		BitField(const char *mem, size_t len);

		/** Destroys the object.
		 */
		~BitField();

		/** Returns bit value at specific position.
		 *
		 * \param index Position of the bit.
		 */
		bool getBit(size_t index);

		/** Returns pointer to the internal data structure.
		 */
		const char *getData() {
			return data;
		};

		/** Returns length of this bitfield.
		 */
		size_t getLength() {
			return length;
		};

		/** Returns number of bits that are not set.
		 */
		size_t getRemaining() {
			return remaining;
		};

		/** Sets all bits.
		 *
		 * \param value Value of the bits.
		 */
		void setAll(bool value = true);

		/** Sets a bit at specific position.
		 *
		 * \param index Position of the bit.
		 * \param value Value of the new bit.
		 */
		void setBit(size_t index, bool value = true);

	private:
		/** Bitfield.
		 */
		char *data;

		/** Length of the bitfield.
		 */
		size_t length;

		/** Number of bits that are still unset.
		 */
		size_t remaining;
};

}; // namespace Core

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
