/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _core_netthread_h
#define _core_netthread_h

#include <tairon/core/thread.h>

namespace Tairon
{

namespace Net
{

class Loop;

}; // namespace Core

}; // namespace Tairon

namespace Tairent
{

namespace Core
{

/** \brief Thread for networking.
 */
class NetThread : public Tairon::Core::Thread
{
	public:
		/** Constructs a thread object with name "net".
		 */
		NetThread();

		/** Destroys the thread.
		 */
		~NetThread();

		/** Quits the loop.
		 */
		void exit();

		/** Executes the thread loop.
		 */
		void *run();

	private:
		/** Whether the thread should exit.
		 */
		bool e;

		/** Network loop;
		 */
		Tairon::Net::Loop *loop;
};

}; // namespace Core

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
