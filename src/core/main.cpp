/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <signal.h>

#include <tairon/core/config.h>
#include <tairon/core/modulemanager.h>
#include <tairon/core/threadmanager.h>
#include <tairon/net/limitmanager.h>
#include <tairon/util/objectmanager.h>

#include "netthread.h"

int main(int argc, char **argv)
{
	// don't crash on broken pipe signal
	signal(SIGPIPE, SIG_IGN);

	Tairon::Core::ThreadManager *threadmanager = 0;
	Tairon::Util::ObjectManager *objectmanager = 0;
	Tairon::Core::ModuleManager *modulemanager = 0;

	Tairent::Core::NetThread *netthread = 0;

	try {
		Tairon::Core::Config config("tairent.xml");
		threadmanager = new Tairon::Core::ThreadManager();
		objectmanager = new Tairon::Util::ObjectManager();
		modulemanager = new Tairon::Core::ModuleManager();

		netthread = new Tairent::Core::NetThread();
		netthread->start();

		modulemanager->initialize();
	} catch (const Tairon::Core::Exception &e) {
		std::cerr << "Caught exception in main: " << (Tairon::Core::String) e << std::endl;
	}

	if (netthread) {
		netthread->exit();
		netthread->join();
	}

	if (modulemanager) {
		modulemanager->unloadModules();
	}

	delete netthread;

	// this is perfectly valid, even if an exception has been thrown - deleting
	// null pointer is correct
	delete modulemanager;
	delete objectmanager;
	delete threadmanager;
}

// vim: ai sw=4 ts=4 noet fdm=marker
