/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/net/loop.h>

#include "netthread.h"

namespace Tairent
{

namespace Core
{

/* {{{ NetThread::NetThread() */
NetThread::NetThread() : Tairon::Core::Thread("net"), e(false)
{
	loop = new Tairon::Net::Loop();
}
/* }}} */

/* {{{ NetThread::~NetThread() */
NetThread::~NetThread()
{
	delete loop;
}
/* }}} */

/* {{{ NetThread::exit() */
void NetThread::exit()
{
	loop->exit();
}
/* }}} */

/* {{{ NetThread::run() */
void *NetThread::run()
{
	loop->run();
	return 0;
}
/* }}} */

}; // namespace Core

}; // namesapce Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
