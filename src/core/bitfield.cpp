/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include "bitfield.h"

namespace Tairent
{

namespace Core
{

/* {{{ BitField::BitField(size_t) */
BitField::BitField(size_t len) : length(len), remaining(length)
{
	size_t s = (len >> 3) + (len & 0x07 ? 1 : 0);
	data = new char[s];
	memset(data, 0, s);
}
/* }}} */

/* {{{ BitField::BitField(const char *, size_t) */
BitField::BitField(const char *mem, size_t len) : length(len), remaining(length)
{
	size_t s = (len >> 3) + (len & 0x07 ? 1 : 0);
	data = new char[s];
	memcpy(data, mem, s);

	for (size_t i = 0; i < len; ++i)
		if (getBit(i))
			remaining -= 1;
}
/* }}} */

/* {{{ BitField::~BitField() */
BitField::~BitField()
{
	delete [] data;
}
/* }}} */

/* {{{ BitField::getBit(size_t) */
bool BitField::getBit(size_t index)
{
	return data[index >> 3] & (0x80 >> (index & 0x07));
}
/* }}} */

/* {{{ BitField::setAll(bool) */
void BitField::setAll(bool value)
{
	size_t s = (length >> 3) + (length & 0x07 ? 1 : 0);
	memset(data, value ? 0xff : 0x00, s);
	remaining = 0;
}
/* }}} */

/* {{{ BitField::setBit(size_t, bool) */
void BitField::setBit(size_t index, bool value)
{
	char orig = data[index >> 3];
	if (value) {
		data[index >> 3] |= 0x80 >> (index & 0x07);
		remaining -= orig ^ data[index >> 3] ? 1 : 0;
	} else {
		data[index >> 3] &= 0x7f >> (index & 0x07);
		remaining += orig ^ data[index >> 3] ? 1 : 0;
	}
}
/* }}} */

}; // namespace Core

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
