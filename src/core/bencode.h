/***************************************************************************
 *                                                                         *
 *   Copyright (C) <year>  <author>                                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _core_bencode_h
#define _core_bencode_h

#include <stdint.h>
#include <list>
#include <map>

#include <tairon/core/exceptions.h>

using Tairon::Core::String;

namespace Tairent
{

namespace Core
{

/** \brief Class that is used to hold bencoded informations.
 */
class BEncode
{
	public:
		/** List type.
		 */
		typedef std::list<BEncode> List;

		/** Dictionary type.
		 */
		typedef std::map<String, BEncode> Map;

		/** BEncode types.
		 */
		enum Type {
			NONE, VALUE, STRING, LIST, MAP
		};

	public:
		/** Constructs BEncode object of type NONE.
		 */
		BEncode();

		/** Constructs BEncode object of type VALUE.
		 *
		 * \param v Value to store in object.
		 */
		BEncode(const int64_t v);

		/** Constructs BEncode object of type STRING.
		 *
		 * \param s String to store in object.
		 */
		BEncode(const String &s);

		/** Copy constructor.
		 *
		 * \param b BEncode object to copy.
		 */
		BEncode(const BEncode &b);

		/** Explicitly constructs BEncode object of given type.
		 *
		 * \param t Type of the object to construct.
		 */
		explicit BEncode(Type t);

		/** Destroys the object.
		 */
		~BEncode();

		/** Converts this BEncode object to LIST and returns it as a reference.
		 */
		BEncode::List &asList();

		/** Converts this BEncode object to MAP and returns it as a reference.
		 */
		BEncode::Map &asMap();

		/** Converts this BEncode object to STRING and returns it as a
		 * reference.
		 */
		String &asString();

		/** Converts this BEncode object to VALUE and returns it as a
		 * reference.
		 */
		int64_t &asValue();

		/** Converts this BEncode object to LIST and returns a constant
		 * reference to it.
		 */
		const BEncode::List &asList() const;

		/** Converts this BEncode object to MAP and returns a constant
		 * reference to it.
		 */
		const BEncode::Map &asMap() const;

		/** Converts this BEncode object to STRING and returns a constant
		 * reference to it.
		 */
		const String &asString() const;

		/** Converts this BEncode object to VALUE and returns a constant
		 * reference to it.
		 */
		const int64_t &asValue() const;

		/** Clears all data from this object and its type sets to NONE.
		 */
		void clear();

		/** Returns true if this object is LIST; otherwise returns false.
		 */
		bool isList() const;

		/** Returns true if this object is MAP; otherwise returns false.
		 */
		bool isMap() const;

		/** Returns true if this object is STRING; otheriwse returns false.
		 */
		bool isString() const;

		/** Returns true if this object is VALUE; otherwise returns false.
		 */
		bool isValue() const;

		/** Returns type of this object.
		 */
		Type getType() const;

		/** Computes SHA1 hash of string representing this object.
		 */
		String computeSHA1() const;

		/** Assigns value of given object to this object and returns a
		 * reference to
		 * it.
		 *
		 * \param b Object to set the value of this object to.
		 */
		BEncode &operator=(const BEncode &b);

		/** If this object is MAP then it returns reference to BEncode object
		 * belonging to given key. If there is no such object then new one is
		 * created with type BEncode::NONE.
		 *
		 * \param key Key of the object to return.
		 */
		BEncode &operator[](const String &key);

		/** If this object is MAP then it returns constant reference to BEncode
		 * object belonging to given key; otherwise raises an exception.
		 *
		 * \param key Key of the object to return.
		 */
		const BEncode &operator[](const String &key) const;

		/** Loads encoded data from stream and stores them to object.
		 *
		 * \param s Input stream to read from.
		 * \param b BEncode object to store data into.
		 */
		friend std::istream &operator>>(std::istream &s, BEncode &b);

		/** Stores given object to stream.
		 *
		 * \param s Output stream to write to.
		 * \param b Object to store.
		 */
		friend std::ostream &operator<<(std::ostream &s, const BEncode &b);

	private:
		/** Reads a string from a stream.
		 *
		 * \param s Input stream from which the data will be read.
		 * \param str String object in which the data will be stored.
		 */
		static bool readString(std::istream &s, String &str);

	private:
		/** Type of this BEnocde.
		 */
		Type type;

		/** Value of this BEncode.
		 */
		union {
			/** Integer value.
			 */
			int64_t value;

			/** String value.
			 */
			String *string;

			/** List value.
			 */
			BEncode::List *list;

			/** Dictionary value.
			 */
			BEncode::Map *map;
		};
};

/** \brief Exception that is raised by BEncode class.
 */
class BEncodeException : public Tairon::Core::Exception
{
	public:
		/** Standard constructor.
		 */
		BEncodeException(const String &desc) : Tairon::Core::Exception(desc) {};

		/** Default destructor.
		 */
		~BEncodeException() {};
};

}; // namespace Core

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
