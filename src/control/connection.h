/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _control_connection_h
#define _control_connection_h

#include <stdint.h>

#include <map>

namespace Tairon
{

namespace Net
{

class Socket;

}; // namespace Net

}; // namespace Tairon


namespace Tairent
{

namespace Core
{

class BEncode;

}; // namespace Core

namespace Control
{

/** \brief Connection between control server and its client.
 */
class Connection
{
	public:
		/** Constructs a Connection object.
		 *
		 * \param fd Descriptor of the socket.
		 */
		Connection(int fd);

		/** Destroys the object.
		 */
		~Connection();

		/** Closes the connection.
		 */
		void close();

		/** Initializes static members.
		 */
		static void initializeStatic();

	public:
		/** Signal emitted when the connection is closed.
		 */
		Tairon::Core::Signal1<void, Connection *> closedSignal;

	private:
		/** Creates response informing about malformed request.
		 *
		 * \param reason Description of the corruption.
		 */
		static Tairent::Core::BEncode createResponseMalformed(const String &reason);

		/** Creates response informing about unkown request.
		 *
		 * \param request The unknown request.
		 */
		static Tairent::Core::BEncode createResponseUnknown(const String &request);

		/** Makes the internal buffer bigger.
		 *
		 * \param size Desired size of the buffer.
		 */
		void enlargeBuffer(uint32_t size);

		/** Reads body of the message.
		 */
		void readMessage();

		/** Reads length of the message.
		 */
		void readMessageLength();

		/** Called when there is something to read from the socket.
		 */
		void readyRead(Tairon::Net::Socket *);

		/** Sends response to the client.
		 *
		 * \param response Bencoded dictionary with response data.
		 */
		void sendResponse(const Tairent::Core::BEncode &response);

		/** Sends lists of served torrents to the client.
		 */
		void sendTorrentList(const Tairent::Core::BEncode &request);

		/** Called when an error occurs.
		 */
		void socketError(Tairon::Net::Socket *, int);

	private:
		/** Buffer for incoming data.
		 */
		char *buffer;

		/** Length of the buffer.
		 */
		uint32_t bufSize;

		/** Mapping for message handlers. The key is value of the request field
		 * and value is its handler.
		 */
		static std::map<String, void (Connection::*)(const Tairent::Core::BEncode &)> msgHandlers;

		/** Length of the incoming message.
		 */
		uint32_t msgLength;

		/** Number of bytes read so far.
		 */
		uint32_t offset;

		/** Method that is called when there is something to read.
		 */
		void (Connection::*readMethod)();

		/** Socket associated with this connection.
		 */
		Tairon::Net::Socket *socket;
};

}; // namespace Control

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
