/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/config.h>
#include <tairon/net/server.h>

#include "controlmodule.h"

#include "connection.h"

using Tairon::Core::String;

namespace Tairent
{

namespace Control
{

/* {{{ ControlModule::ControlModule() */
ControlModule::ControlModule() : Tairon::Core::Module()
{
	Connection::initializeStatic();

	server = new Tairon::Net::Server((*Tairon::Core::Config::self())["control-socket"], 2);
	server->newConnectionSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &ControlModule::newConnection));
	server->ready();
}
/* }}} */

/* {{{ ControlModule::~ControlModule() */
ControlModule::~ControlModule()
{
	for (std::set<Connection *>::iterator it = connections.begin(); it != connections.end(); ++it) {
		(*it)->close();
		delete *it;
	}

	delete server;
}
/* }}} */

/* {{{ ControlModule::connectionClosed(Connection *) */
void ControlModule::connectionClosed(Connection *c)
{
	connections.erase(c);
	delete c;
}
/* }}} */

/* {{{ ControlModule::newConnection(Tairon::Net::Server *, int) */
void ControlModule::newConnection(Tairon::Net::Server *, int fd)
{
	Connection *c = new Connection(fd);
	c->closedSignal.connect(Tairon::Core::methodFunctor(this, &ControlModule::connectionClosed));
	connections.insert(c);
}
/* }}} */

}; // namespace Control

}; // namespace Tairent

EXPORT_MODULE(control, Tairent::Control::ControlModule)

// vim: ai sw=4 ts=4 noet fdm=marker
