/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _control_controlmodule_h
#define _control_controlmodule_h

#include <set>

#include <tairon/core/module.h>
#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

class Server;

}; // namespace Net

}; // namespace Tairon


namespace Tairent
{

namespace Control
{

class Connection;

/** \brief Module for controlling the application.
 */
class ControlModule : public Tairon::Core::Module
{
	public:
		/** Constructs a ControlModule object.
		 */
		ControlModule();

		/** Destroys the object.
		 */
		virtual ~ControlModule();

	private:
		/** Called when a connection has been closed.
		 */
		void connectionClosed(Connection *c);

		/** Called when there's a new connection.
		 */
		void newConnection(Tairon::Net::Server *, int fd);

	private:
		/** List of connections.
		 */
		std::set<Connection *> connections;

		/** Server's unix socket.
		 */
		Tairon::Net::Server *server;
};

}; // namespace Control

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
