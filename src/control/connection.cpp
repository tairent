/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <arpa/inet.h>

#include <sstream>

#include <tairon/net/socket.h>

#include "connection.h"

#include "core/bencode.h"
#include "core/bitfield.h"
#include "main/storage.h"
#include "main/torrentmanager.h"

namespace Tairent
{

namespace Control
{

std::map<String, void (Connection::*)(const Tairent::Core::BEncode &)> Connection::msgHandlers;

/* {{{ Connection::Connection(int) */
Connection::Connection(int fd) : bufSize(1024), offset(0)
{
	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	socket = new Tairon::Net::Socket(fd);
	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::socketError));
	socket->readyReadSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::readyRead));
	socket->ready();

	buffer = new char[bufSize];

	readMethod = &Connection::readMessageLength;
}
/* }}} */

/* {{{ Connection::~Connection() */
Connection::~Connection()
{
	delete [] buffer;
}
/* }}} */

/* {{{ Connection::close() */
void Connection::close()
{
	socket->close();
}
/* }}} */

/* {{{ Connection::createResponseMalformed(const String &) */
Tairent::Core::BEncode Connection::createResponseMalformed(const String &reason)
{
	Tairent::Core::BEncode b(Tairent::Core::BEncode::MAP);

	b["status"] = String("failed");
	b["response"] = String("malformed request");
	b["reason"] = reason;

	return b;
}
/* }}} */

/* {{{ Connection::createResponseUnknown(const String &) */
Tairent::Core::BEncode Connection::createResponseUnknown(const String &request)
{
	Tairent::Core::BEncode b(Tairent::Core::BEncode::MAP);

	b["status"] = String("failed");
	b["response"] = String("unknown request");
	b["request"] = request;

	return b;
}
/* }}} */

/* {{{ Connection::enlargeBuffer(uint32_t) */
void Connection::enlargeBuffer(uint32_t size)
{
	if (bufSize >= size) // nothing to do
		return;

	char *newBuf = new char[size];
	memcpy(newBuf, buffer, bufSize);
	delete [] buffer;
	buffer = newBuf;
}
/* }}} */

/* {{{ Connection::initializeStatic() */
void Connection::initializeStatic()
{
	msgHandlers["torrent list"] = &Connection::sendTorrentList;
}
/* }}} */

/* {{{ Connection::readMessage() */
void Connection::readMessage()
{
	offset += socket->readTo(buffer + offset, msgLength - offset);

	if (offset < msgLength) // incomplete message
		return;

	offset = 0;
	readMethod = &Connection::readMessageLength;

	try {
		std::stringstream s(String(buffer, msgLength));
		Tairent::Core::BEncode b;
		s >> b;

		String request(b["request"].asString());
		if (msgHandlers.count(request)) // is there a handler for this request?
			(this->*(msgHandlers[request]))(b);
		else
			sendResponse(createResponseUnknown(request));
	} catch (const Tairent::Core::BEncode &e) {
		sendResponse(createResponseMalformed((const String &) e));
	}
}
/* }}} */

/* {{{ Connection::readMessageLength() */
void Connection::readMessageLength()
{
	offset += socket->readTo(buffer + offset, 4 - offset);

	if (offset < 4) // incomplete length
		return;

	msgLength = ntohl(*(uint32_t *) buffer);
	enlargeBuffer(msgLength);

	offset = 0;
	readMethod = &Connection::readMessage;
}
/* }}} */

/* {{{ Connection::readyRead(Tairon::Net::Socket *) */
void Connection::readyRead(Tairon::Net::Socket *)
{
	try {
		(this->*(readMethod))();
	} catch (const Tairon::Net::SocketException &) { // connection has been closed
		socket->close();
		closedSignal.emit(this);
	}
}
/* }}} */

/* {{{ Connection::sendResponse(const Tairent::Core::BEncode &) */
void Connection::sendResponse(const Tairent::Core::BEncode &response)
{
	std::stringstream s;
	s << response;
	String data;
	data.reserve(4 + s.str().length());

	uint32_t length = htonl(s.str().length());
	data = String((const char * ) &length, 4);
	data += s.str();

	socket->write(data, true);
}
/* }}} */

/* {{{ Connection::sendTorrentList(const Tairent::Core::BEncode &) */
void Connection::sendTorrentList(const Tairent::Core::BEncode &request)
{
	Tairent::Core::BEncode response(Tairent::Core::BEncode::MAP);
	response["status"] = String("ok");

	Tairent::Main::TorrentManager *manager = Tairent::Main::TorrentManager::self();
	const std::map<String, Tairent::Main::TorrentStruct *> &torrents = manager->getTorrents();

	Tairent::Core::BEncode blist(Tairent::Core::BEncode::LIST);
	Tairent::Core::BEncode::List &list = blist.asList();

	for (std::map<String, Tairent::Main::TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it) {
		Tairent::Core::BEncode torrentInfo(Tairent::Core::BEncode::MAP);
		torrentInfo["info hash"] = it->first;
		torrentInfo["metainfo"] = *it->second->metaInfo;
		torrentInfo["file"] = it->second->torrentFile;

		Tairent::Core::BitField *bitfield = it->second->storage->getBitField();
		torrentInfo["bitfield"] = String(bitfield->getData(), bitfield->getLength() / 8 + (bitfield->getLength() & 0x07 ? 1 : 0));
		torrentInfo["remaining"] = String::number(bitfield->getRemaining());

		list.push_back(torrentInfo);
	}

	response["response"] = blist;

	sendResponse(response);
}
/* }}} */

/* {{{ Connection::socketError(Tairon::Net::Socket *, int) */
void Connection::socketError(Tairon::Net::Socket *, int)
{
	socket->close();
	closedSignal.emit(this);
}
/* }}} */

}; // namespace Control

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
