/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <arpa/inet.h>

#include <sstream>

#include <tairon/core/config.h>
#include <tairon/core/log.h>
#include <tairon/core/thread.h>
#include <tairon/net/socket.h>
#include <tairon/net/timer.h>

#include "azdhtmodule.h"

#include "core/bencode.h"
#include "main/torrentmanager.h"

namespace Tairent
{

namespace AzDHT
{

AzDHTModule *AzDHTModule::azdhtmodule = 0;

/* {{{ AzDHTModule::AzDHTModule() */
AzDHTModule::AzDHTModule() : Tairon::Core::Module(), bufSize(1024), readMethod(0)
{
	buffer = new char[bufSize];

	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	timer = new Tairon::Net::Timer();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &AzDHTModule::reconnect));

	socket = new Tairon::Net::Socket(Tairon::Net::Socket::IPv4, Tairon::Net::Socket::Stream);
	socket->connectedSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &AzDHTModule::connected));
	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &AzDHTModule::socketError));

	try {
		address = (*Tairon::Core::Config::self())["azdht-address"];
	} catch (const Tairon::Core::KeyException &) {
		INFO("Configuration is missing azdht-address record, using default: 127.0.0.1");
		address = "127.0.0.1";
	}

	try {
		port = atoi((*Tairon::Core::Config::self())["azdht-port"]);
	} catch (const Tairon::Core::KeyException &) {
		INFO("Configuration is missing azdht-port record, using default: 7994");
		port = 7994;
	}

	try {
		socket->connect(address, port);
	} catch (const Tairon::Net::SocketException &) {
		INFO("Cannot connect to AzDHT program, retrying");
		connectionError();
	}

	azdhtmodule = this;
}
/* }}} */

/* {{{ AzDHTModule::~AzDHTModule() */
AzDHTModule::~AzDHTModule()
{
	timer->destroy();

	if (socket)
		socket->close();

	for (std::map<String, Tairon::Core::Functor2<void, const String &, const Tairent::Core::BEncode &> *>::iterator it = handlers.begin(); it != handlers.end(); ++it)
		delete it->second;

	delete [] buffer;

	azdhtmodule = 0;
}
/* }}} */

/* {{{ AzDHTModule::connected(Tairon::Net::Socket *) */
void AzDHTModule::connected(Tairon::Net::Socket *)
{
	INFO("Connected to AzDHT");
	offset = 0;
	readMethod = &AzDHTModule::readMessageLength;

	socket->connectedSignal.clear();
	socket->readyReadSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &AzDHTModule::readyRead));
	socket->ready();
}
/* }}} */

/* {{{ AzDHTModule::connectionError() */
void AzDHTModule::connectionError()
{
	INFO("Cannot connect to AzDHT program, retrying");

	socket->close();
	socket = 0;
	readMethod = 0;

	timer->start(60 * 1000, true); // reconnect in 1 minute
}
/* }}} */

/* {{{ AzDHTModule::enlargeBuffer(uint32_t) */
void AzDHTModule::enlargeBuffer(uint32_t size)
{
	if (bufSize >= size) // nothing to do
		return;

	char *newBuf = new char[size];
	memcpy(newBuf, buffer, bufSize);
	delete [] buffer;
	buffer = newBuf;
}
/* }}} */

/* {{{ AzDHTModule::get(const String &key, char flags) */
void AzDHTModule::get(const String &key, char flags)
{
	if (!isConnected())
		return; // do nothing

	Tairent::Core::BEncode b(Tairent::Core::BEncode::MAP);
	b["request"] = String("get");
	b["key"] = key;
	b["flags"] = flags;

	sendQuery(b);
}
/* }}} */

/* {{{ AzDHTModule::isConnected() */
bool AzDHTModule::isConnected()
{
	return readMethod != 0;
}
/* }}} */

/* {{{ AzDHTModule::processMessage(const Tairent::Core::BEncode &) */
void AzDHTModule::processMessage(const Tairent::Core::BEncode &b)
{
	String response;
	String key;

	try {
		response = b["response"].asString();
		key = b["key"].asString();
		b["values"]; // just make sure that there is such item
	} catch (const Tairent::Core::BEncodeException &e) {
		WARNING((const char *) String("Error while processing server's response: " + (const String &) e));
		return;
	}

	if (handlers.count(key))
		(*handlers[key])(key, b["values"]);
}
/* }}} */

/* {{{ AzDHTModule::put(const String &, const String &, char) */
void AzDHTModule::put(const String &key, const String &value, char flags)
{
	if (!isConnected())
		return; // do nothing

	Tairent::Core::BEncode b(Tairent::Core::BEncode::MAP);
	b["request"] = String("put");
	b["key"] = key;
	b["value"] = value;
	b["flags"] = flags;

	sendQuery(b);
}
/* }}} */

/* {{{ AzDHTModule::readMessage() */
void AzDHTModule::readMessage()
{
	offset += socket->readTo(buffer + offset, msgLength - offset);

	if (offset < msgLength) // incomplete message
		return;

	offset = 0;
	readMethod = &AzDHTModule::readMessageLength;

	std::stringstream s(String(buffer, msgLength));
	Tairent::Core::BEncode b;
	try {
		s >> b;
	} catch (const Tairent::Core::BEncode &e) { // shouldn't happen
		WARNING((const char *) String("AzDHT sent corrupted bencode: " + (const String &) e));
		connectionError();
		return;
	}

	processMessage(b);
}
/* }}} */

/* {{{ AzDHTModule::readMessageLength() */
void AzDHTModule::readMessageLength()
{
	offset += socket->readTo(buffer + offset, 4 - offset);

	if (offset < 4) // incomplete length
		return;

	msgLength = ntohl(*(uint32_t *) buffer);
	enlargeBuffer(msgLength);

	offset = 0;
	readMethod = &AzDHTModule::readMessage;
}
/* }}} */

/* {{{ AzDHTModule::readyRead(Tairon::Net::Socket *) */
void AzDHTModule::readyRead(Tairon::Net::Socket *)
{
	try {
		(this->*readMethod)();
	} catch (const Tairon::Net::SocketException &) { // connection has been closed
		connectionError();
	}
}
/* }}} */

/* {{{ AzDHTModule::reconnect() */
void AzDHTModule::reconnect()
{
	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	socket = new Tairon::Net::Socket(Tairon::Net::Socket::IPv4, Tairon::Net::Socket::Stream);
	socket->connectedSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &AzDHTModule::connected));
	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &AzDHTModule::socketError));

	try {
		socket->connect(address, port);
	} catch (const Tairon::Net::SocketException &) {
		connectionError();
	}
}
/* }}} */

/* {{{ AzDHTModule::registerHandler(const String &, Tairon::Core::Functor2<void, const String &, const Tairent::Core::BEncode &> *) */
void AzDHTModule::registerHandler(const String &key, Tairon::Core::Functor2<void, const String &, const Tairent::Core::BEncode &> *handler)
{
	if (handlers.count(key))
		delete handlers[key];
	handlers[key] = handler;
}
/* }}} */

/* {{{ AzDHTModule::remove(const String &) */
void AzDHTModule::remove(const String &key)
{
	if (!isConnected())
		return; // do nothing

	Tairent::Core::BEncode b(Tairent::Core::BEncode::MAP);
	b["request"] = String("remove");
	b["key"] = key;

	sendQuery(b);
}
/* }}} */

/* {{{ AzDHTModule::sendQuery(const Tairent::Core::BEncode &) */
void AzDHTModule::sendQuery(const Tairent::Core::BEncode &query)
{
	std::stringstream s;
	s << query;
	String data;
	data.reserve(4 + s.str().length());

	uint32_t length = htonl(s.str().length());
	data = String((const char * ) &length, 4);
	data += s.str();

	try {
		socket->write(data, true);
	} catch (const Tairon::Net::SocketException &) {
		connectionError();
	}
}
/* }}} */

/* {{{ AzDHTModule::socketError(Tairon::Net::Socket *, int) */
void AzDHTModule::socketError(Tairon::Net::Socket *, int err)
{
	connectionError();
}
/* }}} */

}; // namespace AzDHT

}; // namespace Tairent

EXPORT_MODULE(azdht, Tairent::AzDHT::AzDHTModule)

// vim: ai sw=4 ts=4 noet fdm=marker
