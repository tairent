/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _azdht_azdhtmodule_h
#define _azdht_azdhtmodule_h

#include <map>

#include <tairon/core/module.h>
#include <tairon/core/signals.h>

namespace Tairon
{

namespace Net
{

class Socket;
class Timer;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Core
{

class BEncode;

}; // namespace Core

namespace AzDHT
{

/** \brief Module for communicating with AzDHT program.
 */
class AzDHTModule : public Tairon::Core::Module
{
	public:
		enum Flags {
			SINGLE_VALUE = 0,
			DOWNLOADING = 1,
			SEEDING = 2,
			MULTI_VALUE = 4,
			STATS = 8
		};

		/** Constructs an AzDHTModule object.
		 */
		AzDHTModule();

		/** Destroys the object.
		 */
		virtual ~AzDHTModule();

		/** Queries the DHT for value.
		 */
		void get(const String &key, char flags);

		/** Returns true if we are connected to the DHT; otherwise returns
		 * false.
		 */
		bool isConnected();

		/** Puts new value in the DHT.
		 */
		void put(const String &key, const String &value, char flags);

		/** Registers new handler for incoming values. If there is already
		 * associated handler for the given key then the old handler is
		 * deleted. This module takes ownership of the handler and all
		 * registered handlers are deleted when this module is unloaded.
		 *
		 * \param key Request's key.
		 * \param handler Functor that will be called when values arrive.
		 */
		void registerHandler(const String &key, Tairon::Core::Functor2<void, const String &, const Tairent::Core::BEncode &> *handler);

		/** Removes value for given key from the DHT.
		 */
		void remove(const String &key);

		/** Returns pointer to the instance of this class.
		 */
		static AzDHTModule *self() {
			return azdhtmodule;
		};

	private:
		/** Called when we are connected to the AzDHT server.
		 */
		void connected(Tairon::Net::Socket *);

		/** Closes the socket and tries to reconnect in 1 minute.
		 */
		void connectionError();

		/** Ensures that the internal buffer is large enough.
		 *
		 * \param size Desired size of the buffer.
		 */
		void enlargeBuffer(uint32_t size);

		/** Processes message incoming from the server.
		 *
		 * \param b Message sent by the server.
		 */
		void processMessage(const Tairent::Core::BEncode &b);

		/** Reads body of the message.
		 */
		void readMessage();

		/** Reads length of the message.
		 */
		void readMessageLength();

		/** Called when there is something to read from the socket.
		 */
		void readyRead(Tairon::Net::Socket *);

		/** Reconnects to the AzDHT program.
		 */
		void reconnect();

		/** Sends query to the server.
		 */
		void sendQuery(const Tairent::Core::BEncode &query);

		/** Called when an error occurs.
		 */
		void socketError(Tairon::Net::Socket *, int);

	private:
		/** Address on which AzDHT is listening.
		 */
		String address;

		/** Holds pointer to the instance of this class.
		 */
		static AzDHTModule *azdhtmodule;

		/** Message buffer.
		 */
		char *buffer;

		/** Size of the message buffer.
		 */
		uint32_t bufSize;

		std::map<String, Tairon::Core::Functor2<void, const String &, const Tairent::Core::BEncode &> *> handlers;

		/** Method that reads data from the socket.
		 */
		void (AzDHTModule::*readMethod)();

		/** Length of the message.
		 */
		uint32_t msgLength;

		/** Number of bytes read so far.
		 */
		uint32_t offset;

		/** Port on which AzDHT is listening.
		 */
		uint16_t port;

		/** Connection with the AzDHT program.
		 */
		Tairon::Net::Socket *socket;

		/** Timer for reconnecting to the AzDHT program.
		 */
		Tairon::Net::Timer *timer;
};

}; // namespace AzDHT

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
