/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_trackerclient_h
#define _main_trackerclient_h

namespace Tairent
{

namespace Main
{

class TorrentClient;

/** \brief Base class for trackers.
 */
class TrackerClient
{
	public:
		/** Constructs a TrackerClient object.
		 *
		 * \param c Client that uses this tracker.
		 */
		TrackerClient(TorrentClient *c);

		/** Destroys the object.
		 */
		virtual ~TrackerClient();

		/** Gets list of peers from the tracker.
		 */
		virtual void getPeers();

		/** Stops this client.
		 */
		virtual void stop();

	protected:
		/** Client that uses this tracker.
		 */
		TorrentClient *client;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
