/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006-2007 David Brodsky                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_mainmodule_h
#define _main_mainmodule_h

#include <tairon/core/module.h>

namespace Tairon
{

namespace Net
{

class LimitManager;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Main
{

class HashCheckerThread;
class MainThread;
class TorrentManager;
class TorrentServer;
class TrackerManager;

/** \brief Main BitTorrent client module.
 */
class MainModule : public Tairon::Core::Module
{
	public:
		/** Constructs a MainModule object.
		 */
		MainModule();

		/** Destroys the object.
		 */
		virtual ~MainModule();

	private:
		/** Runs the application. Called when all modules has been loaded.
		 */
		void initialized();

	private:
		/** Thread for hash checking.
		 */
		HashCheckerThread *hashCheckerThread;

		/** Bandwidth limiting.
		 */
		Tairon::Net::LimitManager *limitManager;

		/** Main program's thread.
		 */
		MainThread *mainThread;

		/** Manager of torrents.
		 */
		TorrentManager *torrentManager;

		/** Server incoming connections.
		 */
		TorrentServer *torrentServer;

		/** Manager of factories that creates tracker clients.
		 */
		TrackerManager *trackerManager;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
