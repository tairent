/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_ratemeasurer_h
#define _main_ratemeasurer_h

namespace Tairent
{

namespace Main
{

/** \brief Class for measuring transfer rates.
 */
class RateMeasurer
{
	public:
		/** Constructs a RateMeasurer object.
		 *
		 * \param p Period for measuring.
		 */
		RateMeasurer(double p);

		/** Destroys the object.
		 */
		~RateMeasurer();

		/** Returns current measred rate.
		 */
		double getRate();

		/** Updates the rate with number of trasnferred bytes.
		 */
		void update(unsigned int amount);

	private:
		/** Time of the last update.
		 */
		double lastTime;

		/** Counting period.
		 */
		double period;

		/** Current rate.
		 */
		double rate;

		/** Period's start time.
		 */
		double startTime;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
