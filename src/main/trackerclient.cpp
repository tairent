/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include "trackerclient.h"

namespace Tairent
{

namespace Main
{

/* {{{ TrackerClient::TrackerClient(TorrentClient *) */
TrackerClient::TrackerClient(TorrentClient *c) : client(c)
{
}
/* }}} */

/* {{{ TrackerClient::~TrackerClient() */
TrackerClient::~TrackerClient()
{
}
/* }}} */

/* {{{ TrackerClient::getPeers() */
void TrackerClient::getPeers()
{
}
/* }}} */

/* {{{ TrackerClient::stop() */
void TrackerClient::stop()
{
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
