/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_trackermanager_h
#define _main_trackermanager_h

#include <map>

#include <tairon/core/string.h>

using Tairon::Core::String;

namespace Tairent
{

namespace Main
{

class TorrentClient;
class TrackerClient;
class TrackerClientFactory;

/** \brief TrackerManager keeps informations about trackers for various
 * protocols.
 */
class TrackerManager
{
	public:
		/** Construct a TrackerManager object. This class is a singleton.
		 */
		TrackerManager();

		/** Destroys the object.
		 */
		~TrackerManager();

		/** Returns new tracker client for a torrent.
		 */
		TrackerClient *getTracker(TorrentClient *c);

		/** Registers new protocol for trackers and factory that creates
		 * clients for that protocol.
		 */
		void registerFactory(const String &protocol, TrackerClientFactory *factory);

		/** Returns pointer to the only one instance of this class.
		 */
		static TrackerManager *self() {
			return trackerManager;
		};

		/** Unregisters a factory for specific protocol.
		 */
		void unregisterFactory(const String &protocol);

	private:
		/** Creates a default tracker client. It is used when there is no
		 * tracker factory registered for torrent's tracker.
		 */
		TrackerClient *createDefaultTracker(TorrentClient *client);

	private:
		/** Mapping from protocol name to a factory that creates tracker
		 * clients for such protocol.
		 */
		std::map<String, TrackerClientFactory *> protocols;

		/** Pointer to the only one instance of this class.
		 */
		static TrackerManager *trackerManager;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
