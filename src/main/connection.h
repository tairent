/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_connection_h
#define _main_connection_h

#include <list>
#include <map>
#include <set>

#include <tairon/core/signals.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class Limiter;
class Reader;
class Socket;
class Writer;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Core
{

class BitField;

}; // namespace Core

namespace Main
{

class RateMeasurer;
class TorrentClient;

/** \brief Struct for informations about piece readers.
 *
 * Piece readers are used for reading a piece from the peer. This struct holds
 * a list with all readers used to read the piece and piece's index.
 */
struct PieceReadersStruct
{
	/** Are these readers fake? That means that we want to read the piece buf
	 * we don't want the data.
	 */
	bool fake;

	/** Index of the piece.
	 */
	uint32_t index;

	/** Length of the requested piece.
	 */
	uint32_t length;

	/** List of piece's readers.
	 */
	std::list<Tairon::Net::Reader *> readers;
};

/** \brief Struct with informations about one peer's request.
 */
struct PieceRequestStruct
{
	/** Index of the piece.
	 */
	uint32_t index;

	/** Length of the chunk.
	 */
	uint32_t length;

	/** Offset of the chunk within the piece.
	 */
	uint32_t start;
};

/** \brief Struct for informations about piece writers.
 *
 * Piece writers are used for writing a piece to the peer. This struct holds a
 * list with all writers used to write the piece and piece's index.
 */
struct PieceWritersStruct
{
	/** Index of the piece.
	 */
	uint32_t index;

	/** Length of the piece.
	 */
	uint32_t length;

	/** list of piece's writers.
	 */
	std::list<Tairon::Net::Writer *> writers;
};

/** \brief Connection between two torrent clients.
 *
 * This class handles low level things around BitTorrent protocol.
 */
class Connection
{
	public:
		/** Constructs a Connection object.
		 *
		 * \param fd Descriptor of the socket.
		 */
		Connection(int fd);

		/** Constructs a Connection object. Creates a new socket and connects
		 * it to the peer.
		 *
		 * \param ip IP address of the peer.
		 * \param port Port on which the peer is listening.
		 * \param pID Peer's ID.
		 * \param c Client of this connection.
		 */
		Connection(const String &ip, uint16_t port, const String &pID, TorrentClient *c);

		/** Destroys the obejct.
		 */
		~Connection();

		/** Appends a writer to the command queue. It immediately starts
		 * writing if the queue is empty.
		 */
		void addCommandWriter(Tairon::Net::Writer *writer);

		/** Clears mapping with requested pieces.
		 */
		void clearRequested();

		/** Closes the connection.
		 */
		void close();

		/** Destroys piece readers that are in the queue.
		 */
		void destroyPieceReaders();

		/** Destroys piece writers that are in the queue.
		 */
		void destroyPieceWriters();

		/** Returns this peer's bitfield.
		 */
		Tairent::Core::BitField *getBitField() {
			return bitfield;
		};

		/** Returns current incoming rate.
		 */
		double getIncomingRate();

		/** Returns current outgoing rate.
		 */
		double getOutgoingRate();

		/** Returns length of the piece incoming right now.
		 */
		uint32_t getPieceLength();

		/** Returns current piece reader. If there is no piece reader then this
		 * method returns 0.
		 */
		PieceReadersStruct *getPieceReader() {
			return pieceReaders;
		};

		/** Returns current piece writer. If there is no piece writer then this
		 * method returs 0.
		 */
		PieceWritersStruct *getPieceWriter() {
			return pieceWriters;
		};

		/** Returns peer's ID.
		 */
		const String &getPeerID() {
			return peerID;
		};

		/** Returns pointer to the limiter that limits reading.
		 */
		Tairon::Net::Limiter *getReadingLimiter();

		/** Returns reference to the mapping with requested pieces.
		 */
		const std::map<uint32_t, std::set<uint32_t> > &getRequested() {
			return requests;
		};

		/** Returns number of currently requested pieces.
		 */
		unsigned int getRequestsCount();

		/** Returns pointer to the connection's socket.
		 */
		Tairon::Net::Socket *getSocket() {
			return socket;
		};

		/** Returns time when the peer was unchoked.
		 */
		int getUnchokeTime() {
			return unchokeTime;
		};

		/** Returns pointer to the limiter that limits writing.
		 */
		Tairon::Net::Limiter *getWritingLimiter();

		/** Returns true it we are being choked by the peer; otheriwse returns
		 * false.
		 */
		bool isChoked() {
			return choked;
		};

		/** Returns true if we are interested; otherwise returns false.
		 */
		bool isInterested() {
			return interested;
		};

		/** Returns true if we are choking the peer; otherise returns false.
		 */
		bool isPeerChoked() {
			return peerChoked;
		};

		/** Returns true if the peer is interested; otherwise returns false.
		 */
		bool isPeerInterested() {
			return peerInterested;
		};

		/** Returns true if this connection is snubbed; otherwise returns false.
		 *
		 * TODO: write what snubbed means
		 */
		bool isSnubbed();

		/** Sends choke message to the peer.
		 */
		void sendChoke();

		/** Sends handshake sequence to the peer.
		 *
		 * \param infoHash 20 byte length SHA1 hash of the info part of the torrent.
		 */
		void sendHandshake(const String &infoHash);

		/** Sends have message to the peer.
		 *
		 * \param index Number of the piece that we currently have.
		 */
		void sendHave(uint32_t index);

		/** Sends interested message to the peer.
		 */
		void sendInterested();

		/** Sends not interested message to the peer.
		 */
		void sendNotInterested();

		/** Sends request message to the peer.
		 */
		void sendRequest(uint32_t index, uint32_t start, uint32_t length);

		/** Sends unchoke message to the peer.
		 */
		void sendUnchoke(int time);

		/** Sets peer's ID of this connection. It should be called only when we
		 * are trying to connect to that peer.
		 */
		void setPeerID(const String &pID) {
			peerID = pID;
		};

		/** Sets list of readers that read a piece.
		 */
		void setReaders(PieceReadersStruct *readers);

		/** Sets list of writers that writes a piece.
		 */
		void setWriters(PieceWritersStruct *writers);

	public:
		/** Signal emitted when the connection is closed.
		 */
		Tairon::Core::Signal1<void, Connection *> closedSignal;

	private:
		/** Called when a socket error occured while connecting to the socket.
		 */
		void connectingError(Tairon::Net::Socket *, int);

		/** Called when a Writer has written data to the socket.
		 */
		void dataWritten(Tairon::Net::Writer *writer);

		/** Initializes socket's signals and creates the first reader.
		 */
		void init();

		/** Called when a piece Writer has written data to the socket.
		 */
		void pieceDataWritten(Tairon::Net::Writer *writer);

		/** Reads peer's bitfield.
		 */
		void readBitField(Tairon::Net::Reader *);

		/** Reads cancel message.
		 */
		void readCancel(Tairon::Net::Reader *);

		/** Reads one byte command (i.e. message type).
		 */
		void readCommand(Tairon::Net::Reader *);

		/** Called when a socket error occured while reading by a reader. Emits
		 * the closedSignal.
		 */
		void readerError(Tairon::Net::Reader *, Tairon::Net::Socket *);

		/** Reads first command.
		 */
		void readFirstCommand(Tairon::Net::Reader *);

		/** Reads length of the first command.
		 */
		void readFirstLength(Tairon::Net::Reader *);

		/** Reads handshake sequence.
		 */
		void readHandshake(Tairon::Net::Reader *);

		/** Reads have message.
		 */
		void readHave(Tairon::Net::Reader *);

		/** Reads length of the message.
		 */
		void readLength(Tairon::Net::Reader *);

		/** Reads peer ID.
		 */
		void readPeerID(Tairon::Net::Reader *);

		/** Reads index and start of the piece.
		 */
		void readPiece(Tairon::Net::Reader *);

		/** Reads length of the protocol name. The first byte that arrives
		 * should be 0x13.
		 */
		void readProtocolLength(Tairon::Net::Reader *, size_t);

		/** Reads index, start and length of a peer's request.
		 */
		void readRequest(Tairon::Net::Reader *);

		/** Called when the socket is ready to read.
		 */
		void readyRead(Tairon::Net::Socket *);

		/** Called when the socket is ready to write.
		 */
		void readyWrite(Tairon::Net::Socket *);

		/** Sets the next reader used for reading a piece.
		 */
		void setNextReader(Tairon::Net::Reader *);

		/** Called when the socket has been connected to the peer.
		 */
		void socketConnected(Tairon::Net::Socket *);

		/** Called when a socket error occured. Closes the connection and emits
		 * the closedSignal.
		 */
		void socketError(Tairon::Net::Socket *, int);

		/** Called when a socket error occured while writing to the socket by a
		 * writer. Emits the closedSignal.
		 */
		void writerError(Tairon::Net::Writer *, Tairon::Net::Socket *);

	private:
		/** Peer's bitfield of pieces.
		 */
		Tairent::Core::BitField *bitfield;

		/** Whether we are choked by the peer.
		 */
		bool choked;

		/** Client for this connection.
		 */
		TorrentClient *client;

		/** Length of the current command.
		 */
		uint32_t commandLength;

		/** Queue with unsent commands. The first element may be writing to the
		 * socket.
		 */
		std::list<Tairon::Net::Writer *> commandQueue;

		/** Reader used for reading commands.
		 */
		Tairon::Net::Reader *commandReader;

		/** Whether the current reader should be deleted.
		 */
		bool deleteReader;

		/** Measurer for incoming rate.
		 */
		RateMeasurer *incomingRate;

		/** Are we interested?
		 */
		bool interested;

		/** Time (in miliseconds) of last piece arrival.
		 */
		uint64_t lastPieceTime;

		/** Reader used for reading length of messages.
		 */
		Tairon::Net::Reader *lengthReader;

		/** Measurer for outgoing rate.
		 */
		RateMeasurer *outgoingRate;

		/** Is the peer choked?
		 */
		bool peerChoked;

		/** Peer's ID.
		 */
		String peerID;

		/** Is the peer interested?
		 */
		bool peerInterested;

		/** List of the peer's requests.
		 */
		std::list<PieceRequestStruct> peersRequests;

		/** Index of the piece that is being downloaded now.
		 */
		uint32_t pieceIndex;

		/** List of readers used for reading a piece.
		 */
		PieceReadersStruct *pieceReaders;

		/** List of writers used for writing a piece to the peer.
		 */
		PieceWritersStruct *pieceWriters;

		/** Start of the piece that is being downloaded now.
		 */
		uint32_t pieceStart;

		/** Reader used for reading from the socket.
		 */
		Tairon::Net::Reader *reader;

		/** Method that will read data from the socket.
		 */
		void (Connection::*readMethod)();

		/** Lists of requests sent to the peer.
		 */
		std::map<uint32_t, std::set<uint32_t> > requests;

		/** Socket associated with this connection.
		 */
		Tairon::Net::Socket *socket;

		/** Time when we unchoked the peer.
		 */
		int unchokeTime;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
