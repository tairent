/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/config.h>
#include <tairon/core/log.h>
#include <tairon/net/server.h>
#include <tairon/net/socket.h>

#include "torrentserver.h"

#include "connection.h"

namespace Tairent
{

namespace Main
{

TorrentServer *TorrentServer::torrentserver = 0;

/* {{{ TorrentServer::TorrentServer() */
TorrentServer::TorrentServer()
{
	server = new Tairon::Net::Server(0, atoi((*Tairon::Core::Config::self())["torrent-server-port"]), 2);
	server->newConnectionSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &TorrentServer::newConnection));
	server->ready();

	torrentserver = this;
}
/* }}} */

/* {{{ TorrentServer::~TorrentServer */
TorrentServer::~TorrentServer()
{
	if (server) {
		server->newConnectionSignal.disable();
		delete server;
	}

	torrentserver = 0;
}
/* }}} */

/* {{{ TorrentServer::connectionClosed(Connection *) */
void TorrentServer::connectionClosed(Connection *connection)
{
	pendingConnections.erase(connection);
	delete connection;
}
/* }}} */

/* {{{ TorrentServer::connectionCompleted(Connection *) */
void TorrentServer::connectionCompleted(Connection *connection)
{
	pendingConnections.erase(connection);
	connection->closedSignal.clear();
}
/* }}} */

/* {{{ TorrentServer::newConnection(Tairon::Net::Server *, int) */
void TorrentServer::newConnection(Tairon::Net::Server *, int fd)
{
	DEBUG("new connection");

	Connection *connection = new Connection(fd);
	connection->closedSignal.connect(Tairon::Core::methodFunctor(this, &TorrentServer::connectionClosed));
	pendingConnections.insert(connection);
}
/* }}} */

/* {{{ TorrentServer::stop() */
void TorrentServer::stop()
{
	// delete server
	server->newConnectionSignal.disable();
	delete server;
	server = 0;

	// close pending connections
	for (std::set<Connection *>::const_iterator it = pendingConnections.begin(); it != pendingConnections.end(); ++it) {
		(*it)->close();
		delete *it;
	}
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
