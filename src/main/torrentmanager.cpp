/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006-2007 David Brodsky                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <cstdlib>
#include <ctime>
#include <fstream>

#include <tairon/core/config.h>
#include <tairon/core/exceptions.h>
#include <tairon/core/log.h>
#include <tairon/core/tinyxml.h>
#include <tairon/net/timer.h>

#include "torrentmanager.h"

#include "core/bencode.h"
#include "storage.h"
#include "torrentclient.h"

// Shadow style, modified base64
// 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-
#define TAIRENT_VERSION "010"

static const char *binTable = "0123456789abcdef";
static const char *shadowTable = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-";

namespace Tairent
{

namespace Main
{

TorrentManager *TorrentManager::torrentManager = 0;

/* {{{ TorrentManager::TorrentManager() */
TorrentManager::TorrentManager()
{
	if (torrentManager)
		throw Tairon::Core::SingletonException("Cannot make another instance of Tairent::Main::TorrentManager");

	makeClientID();
	loadTorrents();
	loadResume();

	saveTimer = new Tairon::Net::Timer();
	saveTimer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &TorrentManager::save));
	saveTimer->start(60000); // 1 minute

	torrentManager = this;
}
/* }}} */

/* {{{ TorrentManager::~TorrentManager() */
TorrentManager::~TorrentManager()
{
	saveTimer->destroy();
	torrentManager = 0;
}
/* }}} */

/* {{{ TorrentManager::binToHex(const String &) */
String TorrentManager::binToHex(const String &data)
{
	String ret;
	ret.resize(data.size() * 2);

	for (unsigned int i = 0; i < data.size(); ++i) {
		ret[2 * i] = binTable[(data[i] & 0xf0) >> 4];
		ret[2 * i + 1] = binTable[data[i] & 0x0f];
	}

	return ret;
}
/* }}} */

/* {{{ TorrentManager::destroy() */
void TorrentManager::destroy()
{
	save();
	for (std::map<String, TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it) {
		delete it->second->client;
		delete it->second->storage;
		delete it->second->metaInfo;
		delete it->second;
	}
	torrents.clear();
}
/* }}} */

/* {{{ TorrentManager::getClient(const String &) */
TorrentClient *TorrentManager::getClient(const String &infoHash)
{
	if (torrents.count(infoHash)) {
		TorrentStruct *t = torrents[infoHash];
		if (t->client)
			return t->client;

		t->client = new TorrentClient(t, infoHash, t->storage);
		t->storage = t->client->getStorage();
	}

	return 0; // no client found
}
/* }}} */

/* {{{ TorrentManager::getClientID() */
const String &TorrentManager::getClientID()
{
	return clientID;
}
/* }}} */

/* {{{ TorrentManager::getTorrent(const String &) */
TorrentStruct *TorrentManager::getTorrent(const String &infoHash)
{
	if (torrents.count(infoHash))
		return torrents[infoHash];
	return 0;
}
/* }}} */

/* {{{ TorrentManager::hexToBin(char) */
char TorrentManager::hexToBin(char c)
{
	if ((c >= '0') && (c <= '9'))
		return c - '0';
	return c - 'a' + 10;
}
/* }}} */

/* {{{ TorrentManager::hexToBin(const String &) */
String TorrentManager::hexToBin(const String &data)
{
	String ret;
	ret.resize(data.length() / 2);

	unsigned int i = 0;
	const char *d = data;
	while (*d) {
		ret[i++] = (hexToBin(*d) << 4) | (hexToBin(*(d + 1)));
		d += 2;
	}

	return ret;
}
/* }}} */

/* {{{ TorrentManager::isPrivate(const Tairent::Core::BEncode &) */
bool TorrentManager::isPrivate(const Tairent::Core::BEncode &metaInfo)
{
	try {
		if (metaInfo["info"]["private"].asValue() != 0)
			return true;
	} catch (const Tairent::Core::BEncodeException &) {
		// just ignore invalid torrents
	}

	return false;
}
/* }}} */

/* {{{ TorrentManager::loadMetaInfo(const String &) */
Tairent::Core::BEncode *TorrentManager::loadMetaInfo(const String &filename)
{
	std::ifstream f(filename.contains('/') ? filename : String((*Tairon::Core::Config::self())["torrents-directory"] + '/' + filename), std::ios_base::in);
	if (!f.is_open()) {
		WARNING((const char *) String("Cannot load informations from " + filename));
		return 0;
	}

	Tairent::Core::BEncode *ret = new Tairent::Core::BEncode();
	try {
		f >> (*ret);
	} catch (const Tairent::Core::BEncodeException &e) {
		WARNING((const char *) String("Cannot load metainfo from " + filename + ": " + (const String &) e));
		delete ret;
		return 0;
	}

	return ret;
}
/* }}} */

/* {{{ TorrentManager::loadResume() */
void TorrentManager::loadResume()
{
	TiXmlDocument document;

	if (!document.LoadFile((*Tairon::Core::Config::self())["torrents-directory"] + "/resume.xml"))
		return;

	TiXmlElement *root = document.RootElement();
	if (!root)
		return;

	if (root->ValueStr() != "resume") // invalid file
		return;

	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if (node->Type() != TiXmlNode::ELEMENT)
			continue;
		if (node->ValueStr() == "torrent")
			loadTorrentResume(node);
	}
}
/* }}} */

/* {{{ TorrentManager::loadTorrent(TiXmlNode *) */
void TorrentManager::loadTorrent(TiXmlNode *t)
{
	TiXmlElement *element = t->ToElement();
	const char *data = element->Attribute("file");
	if (!data) {
		WARNING("Missing torrent's filename");
		return;
	}

	INFO((const char *) String(String("Loading informations about ") + data));

	TorrentStruct *torrent = new TorrentStruct;
	torrent->client = 0;
	torrent->storage = 0;

	torrent->torrentFile = data;

	data = element->Attribute("complete");
	if (data && (data[0] == '1'))
		torrent->complete = true;
	else
		torrent->complete = false;

	torrent->metaInfo = loadMetaInfo(torrent->torrentFile);
	if (!torrent->metaInfo)
		return;

	String hash = (*torrent->metaInfo)["info"].computeSHA1();
	torrents[hash] = torrent;
}
/* }}} */

/* {{{ TorrentManager::loadTorrentResume(TiXmlNode *) */
void TorrentManager::loadTorrentResume(TiXmlNode *n)
{
	TiXmlElement *element = n->ToElement();
	const char *data = element->Attribute("hash");
	if (!data)
		return;

	String hash = hexToBin(data);

	if (!torrents.count(hash)) {
		WARNING((const char *) String(String("Unknown torrent hash: ") + data));
		return;
	}

	TorrentStruct *torrent = torrents[hash];

	// load storage if available
	for (TiXmlNode *node = element->FirstChild(); node; node = node->NextSibling()) {
		if (node->Type() != TiXmlNode::ELEMENT)
			continue;
		if (node->ValueStr() == "storage") {
			torrent->storage = new Storage((*torrent->metaInfo)["info"], node);
			if (torrent->complete)
				torrent->storage->complete();
		}
	}
}
/* }}} */

/* {{{ TorrentManager::loadTorrents() */
void TorrentManager::loadTorrents()
{
	TiXmlDocument document;

	if (!document.LoadFile((*Tairon::Core::Config::self())["torrents-directory"] + "/torrents.xml")) {
		ERROR("Cannot load informations about torrents");
		return;
	}

	TiXmlElement *root = document.RootElement();
	if (!root)
		return;

	if (root->ValueStr() != "torrents") // invalid file
		return;

	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if (node->Type() != TiXmlNode::ELEMENT)
			continue;
		if (node->ValueStr() == "torrent")
			loadTorrent(node);
	}
}
/* }}} */

/* {{{ TorrentManager::makeClientID() */
void TorrentManager::makeClientID()
{
	srandom(time(0));

	clientID = "I" TAIRENT_VERSION;
	size_t i = clientID.length();
	clientID.resize(20);

	for (; i < 9; ++i)
		clientID[i] = '-';

	std::ifstream f("/dev/urandom", std::ios_base::in);
	if (f.is_open()) {
		for (; i < 20; ++i)
			clientID[i] = shadowTable[(unsigned int) f.get() % 64];
		f.close();
	} else { // cannot open /dev/urandom? fall back to standard random
		srandom(time(0));
		for (; i < 20; ++i)
			clientID[i] = shadowTable[random() % 64];
	}
}
/* }}} */

/* {{{ TorrentManager::save() */
void TorrentManager::save()
{
	TiXmlDocument document((*Tairon::Core::Config::self())["torrents-directory"] + "/resume.xml");

	TiXmlElement *root = new TiXmlElement("resume");
	document.LinkEndChild(root);

	for (std::map<String, TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it) {
		TiXmlElement *element = new TiXmlElement("torrent");
		element->SetAttribute(String("hash"), binToHex(it->first));
		if (it->second->storage->isComplete())
			element->SetAttribute("complete", 1);
		element->LinkEndChild(it->second->storage->save());
		root->LinkEndChild(element);
	}

	if (!document.SaveFile())
		ERROR("Cannot save informations about torrents.");
}
/* }}} */

/* {{{ TorrentManager::startTorrents() */
void TorrentManager::startTorrents()
{
	for (std::map<String, TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it) {
		TorrentStruct *t = it->second;
		if (t->complete)
			continue;

		if (!t->metaInfo) { // metainfo loading failed
			WARNING((const char *) String("Cannot start torrent " + t->torrentFile));
			continue;
		}

		if (!t->storage) // no fast resume date was loaded
			INFO((const char *) String("No fast-resume data for torrent " + t->torrentFile));

		t->client = new TorrentClient(t, it->first, t->storage);
		t->storage = t->client->getStorage();
	}
}
/* }}} */

/* {{{ TorrentManager::stopTorrents() */
void TorrentManager::stopTorrents()
{
	for (std::map<String, TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it)
		if (it->second->client)
			it->second->client->stop();
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
