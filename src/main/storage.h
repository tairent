/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_storage_h
#define _main_storage_h

#include <list>
#include <map>
#include <set>

#include <tairon/core/signals.h>

#include "core/bencode.h"

class TiXmlElement;
class TiXmlNode;

namespace Tairent
{

namespace Core
{

class BitField;

}; // namespace Core

namespace Main
{

class Connection;
class HashChecker;

struct PieceReadersStruct;
struct PieceWritersStruct;

/** \brief Struct for file descriptor and number of references to the file.
 */
struct FileStruct
{
	/** File descriptor.
	 */
	int fd;

	/** Number of references.
	 */
	unsigned int refCount;
};

/** \brief Struct containing informations about file's offset and its length.
 */
struct FilePositionStruct
{
	/** Filename.
	 */
	String name;

	/** Length of the file.
	 */
	uint64_t length;

	/** Offset of the file from the beginning.
	 */
	uint64_t start;
};

/** \brief Struct for informations about piece mapped to the memory.
 */
struct PieceStruct
{
	/** \brief Struct for part of the piece that belongs to one file.
	 */
	struct FilePieceStruct {
		/** File from which this piece is mapped.
		 */
		const String *filename;

		/** Beginning of the piece in the memory.
		 */
		char *mem;

		/** Real beginning of the piece in the memory due to alignment.
		 */
		void *priv;

		/** Position of the part in the piece.
		 */
		uint32_t start;

		/** Length of the part.
		 */
		uint32_t length;

		/** Real length of the part.
		 */
		uint32_t privLength;
	};

	/** Number of references to the piece.
	 */
	unsigned int refCount;

	/** List of piece's parts.
	 */
	std::list<FilePieceStruct> pieces;
};

/** \brief Struct for piece's requests.
 */
struct RequestStruct
{
	/** Contains indexes of chunks that haven't been requested yet.
	 */
	std::set<uint32_t> queued;

	/** Contains indexes of chunks that have been requested.
	 */
	std::set<uint32_t> requested;
};

/** \brief Storage is an abstraction layer over filesystem.
 */
class Storage
{
	public:
		/** Constructs a new Storage object and loads fast-resume info (if
		 * available).
		 *
		 * \param i Info part of the torrent metainfo;
		 * \param n Node with fast-resume data.
		 */
		Storage(const Tairent::Core::BEncode &i, TiXmlNode *n = 0);

		/** Destroys the object.
		 */
		~Storage();

		/** Adds a bitfield to the availability list.
		 */
		void addBitField(Tairent::Core::BitField *b);

		/** Tells this storage that everything has been downloaded.
		 */
		void complete();

		/** Returns bitfield of this storage.
		 */
		Tairent::Core::BitField *getBitField() {
			return bitfield;
		};

		/** Returns info part of the torrent metainfo.
		 */
		const Tairent::Core::BEncode &getMetaInfo() {
			return info;
		};

		/** Returns informations about piece.
		 */
		PieceStruct *getPiece(uint32_t index);

		/** Returns number of bytes that are still left to download.
		 */
		uint64_t getRemainingSize();

		/** Informs this storage that a peer has something new. Returns true if
		 * the piece is interesting for us.
		 *
		 * \param index Piece index the peer has.
		 */
		bool gotHave(uint32_t index);

		/** Starts downloading of the incoming piece.
		 */
		void gotPiece(uint32_t index, uint32_t start, Connection *c);

		/** Sends requested piece to the peer.
		 *
		 * \param index Index of the piece.
		 * \param start Start of the chunk within the piece.
		 * \param length Length of the requested chunk.
		 * \param c Connection to the peer.
		 */
		void gotRequest(uint32_t index, uint32_t start, uint32_t length, Connection *c);

		/** Returns true if this storage has stored some piece; otherwise
		 * returns false.
		 */
		bool hasSomething();

		/** Returns true if there is nothing left to be downloaded; otherwise
		 * returns false.
		 */
		bool isComplete();

		/** Picks a piece to download. If there is a piece to download then
		 * this method returns true; otherwise returns false.
		 *
		 * \param b Peer's bitfield.
		 * \param index Index of the block.
		 * \param start Start of the piece within the block.
		 * \param length Length of the piece.
		 */
		bool pickPiece(Tairent::Core::BitField *b, uint32_t &index, uint32_t &start, uint32_t &length);

		/** Informs that a requested piece was downloaded.
		 */
		void pieceDownloaded(uint32_t index, uint32_t start);

		/** Informs that a piece requested by a peer has been sent.
		 */
		void pieceSent(uint32_t index);

		/** Removes a bitfield from the availability list.
		 */
		void removeBitField(Tairent::Core::BitField *b);

		/** Informs this storage that we don't expect some pieces to arrive so
		 * it should rerequest them.
		 *
		 * \param r Mapping piece index => list of piece parts to rerequest.
		 */
		void reRequest(const std::map<uint32_t, std::set<uint32_t> > &r);

		/** Returns an XML element that contains informations needed to fast
		 * resume this storage.
		 */
		TiXmlElement *save();

		/** Returns true if the client should be interested; otherwise returns
		 * false. (See BitTorrent protocol for details.)
		 */
		bool shouldBeInterested(Tairent::Core::BitField *b);

		/** Unmaps piece that hasn't been read.
		 */
		void unmapPieces(PieceReadersStruct *readers);

		/** Unmaps piece that hasn't been sent.
		 */
		void unmapPieces(PieceWritersStruct *writers);

	public:
		/** This signal is emitted when a peer has sent an invalid data.
		 */
		Tairon::Core::Signal1<void, Connection *> invalidDataSignal;

		/** Emitted when a piece has been successfuly downloaded and its hash
		 * checked against the one from metainfo.
		 */
		Tairon::Core::Signal1<void, uint32_t> pieceDownloadedSignal;

	private:
		/** Fills files list and computes total length.
		 */
		void buildFilesList();

		/** Closes opened file if it isn't used.
		 */
		void closeFile(const String &name);

		/** Creates a file and truncates it to the specified length.
		 */
		void createFile(const String &name, uint64_t length);

		/** Creates files for this torrent.
		 */
		void createFiles();

		/** Returns length of a chunk.
		 *
		 * \param index Piece number.
		 * \param start Offset of the chunk within the piece.
		 */
		inline uint32_t getChunkLength(uint32_t index, uint32_t start);

		/** Returns length of a piece.
		 *
		 * \param index Piece number.
		 */
		inline uint32_t getPieceLength(uint32_t index);

		/** Called when a recieved piece's hash is the same as in the info.
		 */
		void hashCorrect(uint32_t index, HashChecker *checker);

		/** Called when a recieved piece's hash isn't the same as in the info.
		 */
		void hashIncorrect(uint32_t index, HashChecker *checker);

		/** Converts a path list to a string.
		 */
		String listToString(const Tairent::Core::BEncode::List &l);

		/** Loads fast resume data from an XML node.
		 */
		void load(TiXmlNode *n);

		/** Loads bitfield from a node.
		 */
		void loadBitField(TiXmlNode *n);

		/** Loads information about queued parts from a node.
		 */
		void loadQueued(TiXmlNode *n);

		/** Maps a piece to the memory.
		 */
		void mapPiece(uint32_t index);

		/** Opens a file and returns its descriptor.
		 *
		 * \param filename Path with the file including its name.
		 */
		int openFile(const String &filename);

		/** Creates a permutation of pieces numbers.
		 */
		void scramble();

		/** Unmaps apiece from the memory. The piece isn't actually unmapped
		 * but moved to the cache from which it will be deleted when it will be
		 * needed.
		 */
		void unmapPiece(uint32_t index);

	private:
		/** How many peers have specific piece.
		 */
		unsigned short *availability;

		/** Bitfield of this storage.
		 */
		Tairent::Core::BitField *bitfield;

		/** List of opened files.
		 */
		std::map<String, FileStruct> files;

		/** List of positions of files.
		 */
		std::list<FilePositionStruct> filesPositions;

		/** List of active checkers.
		 */
		std::set<HashChecker *> hashCheckers;

		/** Info part of the torrent metainfo.
		 */
		const Tairent::Core::BEncode &info;

		/** Number of pieces.
		 */
		size_t pieceCount;

		/** Length of one piece.
		 */
		uint32_t pieceLength;

		/** Mapping from piece index to its struct.
		 */
		std::map<uint32_t, PieceStruct *> pieces;

		/** List of active requests. The key is a piece index.
		 */
		std::map<uint32_t, RequestStruct *> requests;

		/** A permutation of length pieceCount.
		 */
		std::list<uint32_t> scrambled;

		/** Sum of lengths of all files.
		 */
		uint64_t totalLength;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
