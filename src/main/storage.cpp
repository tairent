/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <arpa/inet.h>
#include <sys/mman.h>
#include <errno.h>
#include <fcntl.h>

#include <tairon/core/log.h>
#include <tairon/core/tinyxml.h>
#include <tairon/net/ireader.h>
#include <tairon/net/mreader.h>
#include <tairon/net/mwriter.h>
#include <tairon/net/swriter.h>

#include "storage.h"

#include "core/bitfield.h"
#include "connection.h"
#include "hashchecker.h"
#include "torrentmanager.h"

#define min(x, y) (x) < (y) ? x : y

namespace Tairent
{

namespace Main
{

const uint32_t chunkLength = 16384;

/* {{{ Storage::Storage(const Tairent::Core::BEncode &) */
Storage::Storage(const Tairent::Core::BEncode &i, TiXmlNode *n) : info(i)
{
	pieceCount = info["pieces"].asString().length() / 20;
	pieceLength = info["piece length"].asValue();

	buildFilesList();
	createFiles();

	availability = new unsigned short[pieceCount];
	memset(availability, 0, pieceCount * sizeof(unsigned short));

	if (n) // load fast resume
		load(n);
	else // we're starting from the beginning
		bitfield = new Tairent::Core::BitField(pieceCount);

	scramble();
}
/* }}} */

/* {{{ Storage::~Storage() */
Storage::~Storage()
{
	delete bitfield;
	delete [] availability;

	for (std::map<uint32_t, RequestStruct *>::const_iterator it = requests.begin(); it != requests.end(); ++it)
		delete it->second;
}
/* }}} */

/* {{{ Storage::addBitField(Tairent::Core::BitField *) */
void Storage::addBitField(Tairent::Core::BitField *b)
{
	for (size_t i = 0; i < pieceCount; ++i)
		if (b->getBit(i))
			availability[i] +=1;
}
/* }}} */

/* {{{ Storage::buildFilesList() */
void Storage::buildFilesList()
{
	if (info.asMap().count("length")) {
		totalLength = info["length"].asValue();
		FilePositionStruct fs = {
			info["name"].asString(),
			info["length"].asValue(),
			0
		};
		filesPositions.push_back(fs);
	} else {
		totalLength = 0;
		const Tairent::Core::BEncode::List &f = info["files"].asList();
		for (Tairent::Core::BEncode::List::const_iterator it = f.begin(); it != f.end(); ++it) {
			FilePositionStruct fs = {
				listToString((*it)["path"].asList()),
				(*it)["length"].asValue(),
				totalLength
			};
			filesPositions.push_back(fs);
			totalLength += (*it)["length"].asValue();
		}
	}
}
/* }}} */

/* {{{ Storage::closeFile(const String &) */
void Storage::closeFile(const String &filename)
{
	if (!files.count(filename))
		return; // TODO: throw an exception

	FileStruct &fs = files[filename];
	fs.refCount -= 1;
	if (!fs.refCount) {
		close(fs.fd);
		files.erase(filename);
	}
}
/* }}} */

/* {{{ Storage::complete() */
void Storage::complete()
{
	bitfield->setAll();
}
/* }}} */

/* {{{ Storage::createFile(const String &, uint64_t) */
void Storage::createFile(const String &name, uint64_t length)
{
	// TODO: error checking
	int fd = open(name, O_CREAT | O_WRONLY | O_LARGEFILE, 0666);
	ftruncate64(fd, length);
	close(fd);
}
/* }}} */

/* {{{ Storage::createFiles() */
void Storage::createFiles()
{
	DEBUG("Creating files");
	if (info.asMap().count("length")) {
		createFile(info["name"].asString(), info["length"].asValue());
		// TODO: error checking
	} else {
		const Tairent::Core::BEncode::List &f = info["files"].asList();
		mkdir(info["name"].asString(), 0777);
		for (Tairent::Core::BEncode::List::const_iterator it = f.begin(); it != f.end(); ++it) {
			String name = info["name"].asString();
			const Tairent::Core::BEncode::List &path = (*it)["path"].asList();
			Tairent::Core::BEncode::List::const_iterator end = --path.end();
			Tairent::Core::BEncode::List::const_iterator dir;
			for (dir = path.begin(); dir != end; ++dir) {
				name += '/';
				name += dir->asString();
				mkdir(name, 0777);
			}
			name += '/';
			name += dir->asString();
			createFile(name, (*it)["length"].asValue());
		}
	}
}
/* }}} */

/* {{{ Storage::getChunkLength(uint32_t, uint32_t) */
uint32_t Storage::getChunkLength(uint32_t index, uint32_t start)
{
	return min(chunkLength, getPieceLength(index) - start);
}
/* }}} */

/* {{{ Storage::getPiece(uint32_t) */
PieceStruct *Storage::getPiece(uint32_t index)
{
	if (pieces.count(index))
		return pieces[index];
	return 0;
}
/* }}} */

/* {{{ Storage::getPieceLength(uint32_t) */
uint32_t Storage::getPieceLength(uint32_t index)
{
	if (index == (pieceCount - 1)) // last piece
		if (totalLength % pieceLength) // the last piece is shorter
			return totalLength % pieceLength;
		// else: last piece has the same length as any other
	return pieceLength;
}
/* }}} */

/* {{{ Storage::getRemainingSize() */
uint64_t Storage::getRemainingSize()
{
	uint64_t ret = bitfield->getRemaining() * pieceLength;
	if (!bitfield->getBit(pieceCount - 1))
		ret -= pieceLength - getPieceLength(pieceCount - 1);
	return ret;
}
/* }}} */

/* {{{ Storage::gotHave(uint32_t) */
bool Storage::gotHave(uint32_t index)
{
	availability[index] += 1;
	if (bitfield->getBit(index)) // we already have this piece
		return false;
	return true;
}
/* }}} */

/* {{{ Storage::gotPiece(uint32_t, uint32_t, Connection *) */
void Storage::gotPiece(uint32_t index, uint32_t start, Connection *c)
{
	if ((index >= pieceCount) || (start + getChunkLength(index, start) > getPieceLength(index)) || (c->getPieceLength() != getChunkLength(index, start))) {
		WARNING("Client sent an invalid piece");
		invalidDataSignal.emit(c);
		return;
	}

	if (!requests.count(index) || !requests[index]->requested.count(start)) { // discarded
		// create fake reader
		PieceReadersStruct *pr = new PieceReadersStruct;
		pr->fake = true;
		pr->index = index;
		pr->length = getChunkLength(index, start);
		pr->readers.push_back(new Tairon::Net::IReader(c->getPieceLength(), c->getSocket(), c->getReadingLimiter()));
		c->setReaders(pr);
		return;
	}

	// We need to get files to the memory
	mapPiece(index);

	PieceStruct *ps = pieces[index];

	PieceReadersStruct *pr = new PieceReadersStruct;
	pr->fake = false;
	pr->index = index;
	pr->length = getChunkLength(index, start);
	Tairon::Net::Reader *reader;

	std::list<PieceStruct::FilePieceStruct>::iterator it = ps->pieces.begin();
	uint32_t position = start;

	// skip uninteresting files
	while (it->start + it->length < position)
		++it;

	// Just iterate over the mapped files and create readers for them. The
	// difficult part is counting the correct length of the allocated readers.
	// A picture should make this clean:
	//
	// Mapped piece:
	// +-- ... --+------ ... -----------------------------------------+-- ...
	// |         |                                                    |
	// +---... --+------ ... -----------------------------------------+-- ...
	//           ^                       ^                            ^
	//   starting position of      actual position             start + length
	// the file within a piece           |<- length of the chunk if ->|
	//                                   | the file is shorter than   |
	//                                   | what we need; otherwise we |
	//                                   | use the remaining count    |

	uint32_t remaining = c->getPieceLength();
	while (remaining) {
		uint32_t len = min(it->start + it->length - position, remaining);
		reader = new Tairon::Net::MReader(it->mem + position - it->start, len, c->getSocket(), c->getReadingLimiter());
		pr->readers.push_back(reader);
		remaining -= len;
		position += len;
		++it;
	}

	c->setReaders(pr);
}
/* }}} */

/* {{{ Storage::gotRequest(uint32_t, uint32_t, uint32_t, Connection *) */
void Storage::gotRequest(uint32_t index, uint32_t start, uint32_t length, Connection *c)
{
	if ((index >= pieceCount) || (start + length > getPieceLength(index))) {
		WARNING("Client sent an invalid request");
		invalidDataSignal.emit(c);
		return;
	}

	// map the piece to the memory
	mapPiece(index);

	PieceStruct *ps = pieces[index];

	PieceWritersStruct *writers = new PieceWritersStruct;
	writers->index = index;
	writers->length = length;
	Tairon::Net::Writer *writer;

	std::list<PieceStruct::FilePieceStruct>::iterator it = ps->pieces.begin();
	uint32_t position = start;

	// skip uninteresting files
	while (it->start + it->length < position)
		++it;

	// create writer for the message
	char header[13]; // 4 bytes msg length, 1 byte command and 2 x 4 bytes for index and start
	uint32_t num = htonl(length + 9);
	memcpy(header, (const char *) (&num), 4);
	header[4] = 7; // piece type
	num = htonl(index);
	memcpy(header + 5, (const char *) (&num), 4);
	num = htonl(start);
	memcpy(header + 9, (const char *) (&num), 4);
	writer = new Tairon::Net::SWriter(String(header, 13), c->getSocket(), c->getWritingLimiter());
	writers->writers.push_back(writer);

	// create writers for the piece
	uint32_t remaining = length;
	while (remaining) {
		uint32_t len = min(it->start + it->length - position, remaining);
		writer = new Tairon::Net::MWriter(it->mem + position - it->start, len, c->getSocket(), c->getWritingLimiter());
		writers->writers.push_back(writer);
		remaining -= len;
		position += len;
		++it;

		if ((it == ps->pieces.end()) && remaining) {
			// we are at the end of the piece and there are still some bytes left?
			// destroy created writers and unmap piece
			for (std::list<Tairon::Net::Writer *>::iterator it = writers->writers.begin(); it != writers->writers.end(); ++it)
				delete *it;
			delete writers;

			unmapPiece(index);

			return;
		}
	}

	// send it to the peer
	c->setWriters(writers);
}
/* }}} */

/* {{{ Storage::hashCorrect(uint32_t, HashChecker *) */
void Storage::hashCorrect(uint32_t index, HashChecker *checker)
{
	hashCheckers.erase(checker);
	checker->destroy();
	bitfield->setBit(index);
	pieceDownloadedSignal.emit(index);
	unmapPiece(index);
}
/* }}} */

/* {{{ Storage::hashIncorrect(uint32_t, HashChecker *) */
void Storage::hashIncorrect(uint32_t index, HashChecker *checker)
{
	scrambled.push_back(index);
	DEBUG("hash incorrect");
	hashCheckers.erase(checker);
	checker->destroy();
	unmapPiece(index);
}
/* }}} */

/* {{{ Storage::hasSomething() */
bool Storage::hasSomething()
{
	if (pieceCount - bitfield->getRemaining())
		return true;
	return false;
}
/* }}} */

/* {{{ Storage::isComplete() */
bool Storage::isComplete()
{
	return !bitfield->getRemaining();
}
/* }}} */

/* {{{ Storage::listToString(const Tairent::Core::BEncode::List &) */
String Storage::listToString(const Tairent::Core::BEncode::List &l)
{
	String ret = info["name"].asString();
	for (Tairent::Core::BEncode::List::const_iterator it = l.begin(); it != l.end(); ++it) {
		ret += '/';
		ret += it->asString();
	}
	return ret;
}
/* }}} */

/* {{{ Storage::load(TiXmlNode *) */
void Storage::load(TiXmlNode *n)
{
	TiXmlElement *root = n->ToElement();

	for (TiXmlNode *node = root->FirstChild(); node; node = node->NextSibling()) {
		if (node->Type() != TiXmlNode::ELEMENT)
			continue;

		if (node->ValueStr() == "bitfield")
			loadBitField(node);
		else if (node->ValueStr() == "queued")
			loadQueued(node);
	}
}
/* }}} */

/* {{{ Storage::loadBitField(TiXmlNode *) */
void Storage::loadBitField(TiXmlNode *n)
{
	TiXmlElement *element = n->ToElement();

	const char *t = element->GetText();
	if (!t) {
		WARNING("Cannot load bitfield");
		return;
	}

	bitfield = new Tairent::Core::BitField(TorrentManager::hexToBin(t), pieceCount);
}
/* }}} */

/* {{{ Storage::loadQueued(TiXmlNode *) */
void Storage::loadQueued(TiXmlNode *n)
{
	TiXmlElement *element = n->ToElement();

	const char *i = element->Attribute("index");
	if (!i) {
		WARNING("Missing index for queued pieces");
		return;
	}

	uint32_t index;
	long long num;

	errno = 0;
	num = strtoll(i, 0, 10); // get the index
	if (errno) { // error while converting
		WARNING("Error while converting piece index");
		return;
	} else if ((num < 0) || (num >= pieceCount)) { // out of range
			WARNING((const char *) String("Invalid piece index: " + String::number(num)));
			return;
	}
	index = num & 0xffffffff;

	const char *t = element->GetText();
	if (!t) {
		WARNING((const char *) String("No queued piece for index: " + String::number(index)));
		return;
	}

	char *end;
	uint32_t piece;
	RequestStruct *rs = new RequestStruct;

	// go through the list
	do {
		errno = 0;
		num = strtoll(t, &end, 10);
		if (errno) {
			delete rs;
			WARNING((const char *) String("Cannot load queued index: " + String::number(index)));
			return;
		}
		piece = num & 0xffffffff;

		rs->queued.insert(piece);

		t = end + 1; // skip ','
	} while (!errno && *end);

	requests[index] = rs;
}
/* }}} */

/* {{{ Storage::mapPiece(uint32_t) */
void Storage::mapPiece(uint32_t index)
{
	// TODO: index checking

	if (pieces.count(index)) {
		pieces[index]->refCount++;
		return;
	}

	uint64_t currentPos = index * pieceLength;
	uint64_t finalPos = currentPos + getPieceLength(index);

	std::list<FilePositionStruct>::iterator it = filesPositions.begin();

	// find the first file that contains our index
	while ((it->start + it->length) < currentPos)
		++it;

	PieceStruct *ps = new PieceStruct;
	PieceStruct::FilePieceStruct fps;

	// Now begins the tricky part. We need few informations that will be used
	// later for creating readers/writers. The first case is somewhat special,
	// because a piece can start (and usually does) in the middle of a file.
	// The second part just maps pieces at the beginning of the files, so we
	// don't need to do the getpagesize() stuff.

	int fd = openFile(it->name);
	fps.filename = &it->name;

	// Length of the allocated block. Because mmapped block must start at a
	// multiple of page size we need to add this size. This size will be passed
	// to the unmap function.
	// The first part computes the ramaining length of the file aligned to the
	// page size. The second one just aligns length of a piece to the page
	// size.
	fps.privLength = min(it->start + it->length - currentPos + ((currentPos - it->start) % getpagesize()), pieceLength + (currentPos - it->start) % getpagesize());

	// Map the piece! Length of the piece was counted above. Offset is just a
	// current position of the piece in bytes minus starting position of the
	// file and it is aligned to the page size.
	fps.priv = mmap64(0, fps.privLength, PROT_READ | PROT_WRITE, MAP_SHARED, fd, currentPos - it->start - (currentPos - it->start) % getpagesize());

	// Real starting position. We need to add previously subtracted page size
	// alignment.
	fps.mem = (char *) fps.priv + (currentPos - it->start) % getpagesize();

	// The first piece always starts at position 0.
	fps.start = 0;

	// Length of the piece is the length of the mapped block minus page size
	// alignment.
	fps.length = fps.privLength - (currentPos - it->start) % getpagesize();

	// And, at last, add this piece to the list.
	ps->pieces.push_back(fps);

	// Map rest of the piece. We can reuse the fps struct because a copy of it
	// is already in the list.
	currentPos += fps.length;

	while (currentPos < finalPos) {
		++it;
		int fd = openFile(it->name);
		fps.filename = &it->name;
		fps.start += fps.length;
		fps.length = min(finalPos - currentPos, it->length);
		fps.privLength = fps.length;
		fps.priv = mmap64(0, fps.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		fps.mem = (char *) fps.priv;
		ps->pieces.push_back(fps);
		currentPos += fps.length;
	}

	ps->refCount = 1;

	pieces[index] = ps;
}
/* }}} */

/* {{{ Storage::openFile(const String &) */
int Storage::openFile(const String &filename)
{
	if (files.count(filename)) {
		FileStruct &fs = files[filename];
		++fs.refCount;
		return fs.fd;
	}
	FileStruct fs = {
		open(filename, O_CREAT | O_RDWR | O_LARGEFILE, 0666),
		1
	};
	// TODO: error checking
	files[filename] = fs;
	return fs.fd;
}
/* }}} */

/* {{{ Storage::pickPiece(Tairent::Core::BitField *, uint32_t &, uint32_t &, uint32_t &) */
bool Storage::pickPiece(Tairent::Core::BitField *b, uint32_t &index, uint32_t &start, uint32_t &length)
{
	// first try to find a piece in already requested ones.
	for (std::map<uint32_t, RequestStruct *>::iterator it = requests.begin(); it != requests.end(); ++it)
		if (b->getBit(it->first)) {
			if (!it->second->queued.size())
				continue;
			start = *(it->second->queued.begin());
			it->second->queued.erase(start);
			it->second->requested.insert(start);
			length = getChunkLength(it->first, start);
			index  = it->first;
			return true;
		}

	for (std::list<uint32_t>::iterator it = scrambled.begin(); it != scrambled.end(); ++it)
		if (b->getBit(*it) && !requests.count(*it)) {
			RequestStruct *rs = new RequestStruct;

			uint32_t pieceLength = getPieceLength(*it);
			uint32_t st = min(chunkLength, pieceLength);
			uint32_t len;

			while (st < pieceLength) {
				len = min(chunkLength, pieceLength - st);
				rs->queued.insert(st);
				st += len;
			}

			index = *it;
			start = 0;
			length = getChunkLength(*it, 0);

			rs->requested.insert(0);
			requests[index] = rs;

			return true;
		}

	return false;
}
/* }}} */

/* {{{ Storage::pieceDownloaded(uint32_t, uint32_t) */
void Storage::pieceDownloaded(uint32_t index, uint32_t start)
{
	if (!requests.count(index))
		return; // TODO: discarded bytes

	RequestStruct *rs = requests[index];
	if (!rs->requested.count(start))
		return; // TODO: discarded bytes

	rs->requested.erase(start);

	if (!rs->requested.size() && !rs->queued.size()) {
		delete rs;
		requests.erase(index);
		scrambled.remove(index); // don't download this piece

		HashChecker *checker = new HashChecker(index, this);
		hashCheckers.insert(checker);
		Tairon::Core::Thread *current = Tairon::Core::Thread::current();
		checker->hashCorrectSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Storage::hashCorrect));
		checker->hashIncorrectSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Storage::hashIncorrect));
		checker->check();
	} else
		unmapPiece(index);
}
/* }}} */

/* {{{ Storage::pieceSent(uint32_t) */
void Storage::pieceSent(uint32_t index)
{
	unmapPiece(index);
}
/* }}} */

/* {{{ Storage::removeBitField(Tairent::Core::BitField *) */
void Storage::removeBitField(Tairent::Core::BitField *b)
{
	if (!b) // We can get this from a connection that hasn't been fully established
		return;

	for (size_t i = 0; i < pieceCount; ++i)
		if (b->getBit(i))
			availability[i] -= 1;
}
/* }}} */

/* {{{ Storage::reRequest(const std::map<uint32_t, std::set<uint32_t> > &) */
void Storage::reRequest(const std::map<uint32_t, std::set<uint32_t> > &r)
{
	for (std::map<uint32_t, std::set<uint32_t> >::const_iterator it1 = r.begin(); it1 != r.end(); ++it1) {
		RequestStruct *rs;

		if (!requests.count(it1->first)) {
			rs = new RequestStruct;
			requests[it1->first] = rs;
		} else
			rs = requests[it1->first];

		for (std::set<uint32_t>::const_iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {
			rs->queued.insert(*it2);
			rs->requested.erase(*it2);
		}
	}
}
/* }}} */

/* {{{ Storage::save() */
TiXmlElement *Storage::save()
{
	TiXmlElement *element = new TiXmlElement("storage");

	// store bitfield
	TiXmlElement *child = new TiXmlElement("bitfield");
	size_t bitFieldLength = bitfield->getLength();
	child->LinkEndChild(new TiXmlText(TorrentManager::binToHex(String(bitfield->getData(), bitFieldLength / 8 + (bitFieldLength % 8 ? 1 : 0)))));
	element->LinkEndChild(child);

	// store list of parts that we don't have
	for (std::map<uint32_t, RequestStruct *>::const_iterator it1 = requests.begin(); it1 != requests.end(); ++it1) {
		String queued;

		// store parts that haven't been requested
		for (std::set<uint32_t>::const_iterator it2 = it1->second->queued.begin(); it2 != it1->second->queued.end(); ++it2)
			queued += String::number(*it2) + ',';

		// store parts that have been requested but we don't have them yet
		for (std::set<uint32_t>::const_iterator it2 = it1->second->requested.begin(); it2 != it1->second->requested.end(); ++it2)
			queued += String::number(*it2) + ',';

		// remove trailing ','
		queued.resize(queued.size() - 1);

		child = new TiXmlElement("queued");
		child->SetAttribute(String("index"), String::number(it1->first));
		child->LinkEndChild(new TiXmlText(queued));
		element->LinkEndChild(child);
	}

	return element;
}
/* }}} */

/* {{{ Storage::scramble() */
void Storage::scramble()
{
	uint32_t *buf = new uint32_t[pieceCount];

	uint32_t pos = 0;
	for (uint32_t i = 0; i < pieceCount; ++i)
		if (!bitfield->getBit(i)) // we don't have this piece yet, add it to the list
			buf[pos++] = i;
/*
	for (uint32_t i = 0; i < pieceCount; ++i) {
		uint32_t orig = buf[i];
		uint32_t pos = random() % pieceCount;
		buf[i] = buf[pos];
		buf[pos] = orig;
	}
*/
	scrambled.clear();
	for (uint32_t i = 0; i < pos; ++i)
		scrambled.push_back(buf[i]);

	delete [] buf;
}
/* }}} */

/* {{{ Storage::shouldBeInterested(Tairent::Core::BitField *) */
bool Storage::shouldBeInterested(Tairent::Core::BitField *b)
{
	for (size_t i = 0; i < pieceCount; ++i)
		if (b->getBit(i) && !bitfield->getBit(i))
			return true;
	return false;
}
/* }}} */

/* {{{ Storage::unmapPiece(uint32_t) */
void Storage::unmapPiece(uint32_t index)
{
	// check if it is mapped
	if (!pieces.count(index)) {
		ERROR((const char *) String("Unmapping piece that isn't mapped: " + String::number(index)));
		return;
	}

	PieceStruct *ps = pieces[index];

	ps->refCount -= 1;

	if (ps->refCount) // don't unmap piece that is needed
		return;

	for (std::list<PieceStruct::FilePieceStruct>::iterator it = ps->pieces.begin(); it != ps->pieces.end(); ++it) {
		munmap(it->priv, it->privLength);
		closeFile(*it->filename);
	}

	delete ps;
	pieces.erase(index);
}
/* }}} */

/* {{{ Storage::unmapPieces(PieceReadersStruct *) */
void Storage::unmapPieces(PieceReadersStruct *readers)
{
	if (!readers)
		return;
	if (readers->fake) // this piece hasn't been mapped
		return;
	unmapPiece(readers->index);
}
/* }}} */

/* {{{ Storage::unmapPieces(PieceWritersStruct *) */
void Storage::unmapPieces(PieceWritersStruct *writers)
{
	if (!writers)
		return;
	unmapPiece(writers->index);
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
