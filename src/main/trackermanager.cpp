/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/exceptions.h>
#include <tairon/core/log.h>
#include <tairon/util/regexp.h>

#include "trackermanager.h"

#include "core/bencode.h"
#include "torrentclient.h"
#include "trackerclientfactory.h"

namespace Tairent
{

namespace Main
{

TrackerManager *TrackerManager::trackerManager = 0;

/* {{{ TrackerManager::TrackerManager() */
TrackerManager::TrackerManager()
{
	if (trackerManager)
		throw Tairon::Core::SingletonException("Cannot make another instance of Tairent::Main::TrackerManager");

	trackerManager = this;
}
/* }}} */

/* {{{ TrackerManager::~TrackerManager() */
TrackerManager::~TrackerManager()
{
	trackerManager = 0;
}
/* }}} */

/* {{{ TrackerManager::createDefaultTracker(TorrentClient *) */
TrackerClient *TrackerManager::createDefaultTracker(TorrentClient *client)
{
	return 0;
}
/* }}} */

/* {{{ TrackerManager::getTracker(TorrentClient *) */
TrackerClient *TrackerManager::getTracker(TorrentClient *client)
{
	const Tairent::Core::BEncode &metaInfo = client->getMetaInfo();
	try {
		Tairon::Util::RegExp r("([[:alpha:]]*)://([[:alnum:]\\.-]+)(:([0-9]+))?(.*)");
		if (r.match(metaInfo["announce"].asString()) == -1)
			return createDefaultTracker(client);

		String protocol = r.matched(1);
		if (!protocols.count(protocol))
			return createDefaultTracker(client);

		return protocols[protocol]->createTrackerClient(client);
	} catch (const Tairent::Core::BEncodeException &) {
		// there is no announce item in metainfo
		return createDefaultTracker(client);
	}

	return 0;
}
/* }}} */

/* {{{ TrackerManager::registerFactory(const String &, TrackerClientFactory *) */
void TrackerManager::registerFactory(const String &protocol, TrackerClientFactory *factory)
{
	protocols[protocol] = factory;
	INFO((const char *) String("Registered factory for protocol " + protocol));
}
/* }}} */

/* {{{ TrackerManager::unregisterFactory(const String &) */
void TrackerManager::unregisterFactory(const String &protocol)
{
	protocols.erase(protocol);
	INFO((const char *) String("Unregistered factory for protocol " + protocol));
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
