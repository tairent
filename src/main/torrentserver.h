/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_torrentserver_h
#define _main_torrentserver_h

#include <set>

namespace Tairon
{

namespace Net
{

class Server;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Main
{

class Connection;

/** \brief Server accepting incomming connections for torrents.
 */
class TorrentServer
{
	public:
		/** Starts the server on port specified by config key
		 * "torrent-server-port".
		 */
		TorrentServer();

		/** Destroys the server.
		 */
		~TorrentServer();

		/** Tells the server that a handshake sequence has been successfuly
		 * completed.
		 */
		void connectionCompleted(Connection *connection);

		/** Returns pointer to the instance of this class.
		 */
		static TorrentServer *self() {
			return torrentserver;
		};

		/** Stops the server.
		 */
		void stop();

	private:
		/** Called when a pending connection has been closed.
		 */
		void connectionClosed(Connection *connection);

		/** Called when a new connection arrives.
		 */
		void newConnection(Tairon::Net::Server *, int fd);

	private:
		/** Set of connections that aren't fully established yet.
		 */
		std::set<Connection *> pendingConnections;

		/** Server object for accepting incoming connections.
		 */
		Tairon::Net::Server *server;

		/** Pointer to the instance of this class.
		 */
		static TorrentServer *torrentserver;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
