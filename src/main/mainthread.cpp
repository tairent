/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <cstdio>
#include <signal.h>

#include <tairon/core/log.h>

#include "mainthread.h"

static unsigned int todoCounter = 1;

/* {{{ signalHandler(int) */
static void signalHandler(int)
{
	INFO("Signal caught, terminating");

	signal(SIGINT, SIG_DFL);
	todoCounter -= 1;
}
/* }}} */

namespace Tairent
{

namespace Main
{

MainThread *MainThread::mainThread = 0;

/* {{{ MainThread::MainThread() */
MainThread::MainThread() : Tairon::Core::Thread("main")
{
	signal(SIGINT, signalHandler);
	signal(SIGUSR1, signalHandler);
}
/* }}} */

/* {{{ MainThread::~MainThread() */
MainThread::~MainThread()
{
}
/* }}} */

/* {{{ MainThread::operationDone() */
void MainThread::operationDone()
{
	todoCounter += 1;
}
/* }}} */

/* {{{ MainThread::operationInProgress() */
void MainThread::operationInProgress()
{
	todoCounter -= 1;
}
/* }}} */

/* {{{ MainThread::run() */
void *MainThread::run()
{
	while (todoCounter) {
		waitAndCallFunctors(1, 0);
		fflush(stdout);
	}

	return 0;
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
