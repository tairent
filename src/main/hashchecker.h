/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_hashchecker_h
#define _main_hashchecker_h

#include <list>

#include <tairon/core/signals.h>

namespace Tairent
{

namespace Main
{

using Tairon::Core::String;

struct PieceStruct;

class Storage;

/** \brief HashChecker is used to check a piece against its hash.
 */
class HashChecker
{
	public:
		/** Constructs a HashChecker object.
		 *
		 * \param i Number of the piece to check.
		 * \param s Storage with the piece.
		 */
		HashChecker(uint32_t i, Storage *s);

		/** Destroys the object.
		 */
		~HashChecker();

		/** Checks the piece's hash. This job is done in a different thread, so
		 * it shouldn't slow down main processing. When the job is done a
		 * specific signal is emitted. This method just creates a functor to
		 * call checkPiece() in a different thread.
		 *
		 * \sa hashCorrectSignal
		 * \sa hashIncorrectSignal
		 */
		void check();

		/** Puts this checker to the queue for deletion.
		 */
		void destroy();

		/** Returns index of the piece that is checked by this object.
		 */
		uint32_t getIndex() {
			return index;
		};

	public:
		/** This signal is emitted when the piece has the correct hash.
		 */
		Tairon::Core::DSignal2<uint32_t, HashChecker *> hashCorrectSignal;

		/** This signal is emitted when the piece has an incorrect hash.
		 */
		Tairon::Core::DSignal2<uint32_t, HashChecker *> hashIncorrectSignal;

	private:
		/** Checks the piece.
		 */
		void checkPiece();

	private:
		/** Disabler for functor that calls checkPiece() method.
		 */
		Tairon::Core::FunctorDisabler *disabler;

		/** Index of the piece.
		 */
		uint32_t index;

		/** Informations about the piece.
		 */
		PieceStruct *piece;

		/** Hash of the piece from the torrent's metainfo.
		 */
		String pieceHash;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
