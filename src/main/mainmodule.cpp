/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/log.h>
#include <tairon/core/modulemanager.h>
#include <tairon/core/signals.h>
#include <tairon/core/threadmanager.h>
#include <tairon/net/limiter.h>
#include <tairon/net/limitmanager.h>

#include "mainmodule.h"

#include "hashcheckerthread.h"
#include "mainthread.h"
#include "torrentmanager.h"
#include "torrentserver.h"
#include "trackermanager.h"

namespace Tairent
{

namespace Main
{

extern Tairon::Net::Limiter *rlimiter;
extern Tairon::Net::Limiter *wlimiter;

/* {{{ MainModule::MainModule() */
MainModule::MainModule() : Tairon::Core::Module()
{
	// build thread first, because it may be needed by other objects
	mainThread = new MainThread();
	Tairon::Core::ThreadManager::self()->setMainThread(mainThread);

	hashCheckerThread = new HashCheckerThread();

	trackerManager = new TrackerManager();
	torrentManager = new TorrentManager();
	torrentServer = new TorrentServer();

	limitManager = new Tairon::Net::LimitManager();

	rlimiter = new Tairon::Net::Limiter(500000);
	wlimiter = new Tairon::Net::Limiter(500000);

	Tairon::Core::ModuleManager::self()->initializedSignal->connect(Tairon::Core::methodFunctor(this, &MainModule::initialized));
}
/* }}} */

/* {{{ MainModule::~MainModule() */
MainModule::~MainModule()
{
	delete rlimiter;
	delete wlimiter;
	delete limitManager;
	delete hashCheckerThread;
	delete mainThread;
	delete torrentServer;
	delete torrentManager;
	delete trackerManager;
}
/* }}} */

/* {{{ MainModule::initialized() */
void MainModule::initialized()
{
	INFO("Starting application");
	torrentManager->startTorrents();

	hashCheckerThread->start();

	mainThread->run();

	torrentServer->stop();
	torrentManager->stopTorrents();

	mainThread->run(); // wait for objects to clean up

	hashCheckerThread->stop();
	hashCheckerThread->join();

	torrentManager->destroy();
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

EXPORT_MODULE(main, Tairent::Main::MainModule)

// vim: ai sw=4 ts=4 noet fdm=marker
