/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_mainthread_h
#define _main_mainthread_h

#include <tairon/core/thread.h>

namespace Tairent
{

namespace Main
{

/** \brief Main applications thread.
 */
class MainThread : public Tairon::Core::Thread
{
	public:
		/** Constructs a MainThread object.
		 */
		MainThread();

		/** Destroys the object.
		 */
		virtual ~MainThread();

		/** Called during clean-up stage that one operation has been completed.
		 */
		void operationDone();

		/** Called during clean-up stage to wait for one more operation to
		 * complete.
		 */
		void operationInProgress();

		/** Runs the main application loop.
		 */
		virtual void *run();

		/** Returns pointer to the instance of this class.
		 */
		static MainThread *self() {
			return mainThread;
		};

	private:
		/** Pointer to the instance of this class.
		 */
		static MainThread *mainThread;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
