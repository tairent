/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006-2007 David Brodsky                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_torrentmanager_h
#define _main_torrentmanager_h

#include <map>

#include <tairon/core/string.h>

using Tairon::Core::String;

class TiXmlNode;

namespace Tairon
{

namespace Net
{

class Timer;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Core
{

class BEncode;

}; // namespace Core

namespace Main
{

class Storage;
class TorrentClient;

/** \brief Holds informations about managed torrents.
 *
 * This class is a singleton.
 */
struct TorrentStruct
{
	/** Client for this torrent. If there is no such client then it is zero.
	 */
	TorrentClient *client;

	/** This client's storage. It is used for fast-resuming.
	 */
	Storage *storage;

	/** Meta info of the torrent. If the metainfo has not been loaded yet then
	 * it is zero.
	 */
	Tairent::Core::BEncode *metaInfo;

	/** Whether this torrent is completely downloaded.
	 */
	bool complete;

	/** Filename of torrent.
	 */
	String torrentFile;
};

/** \brief TorrentManager keeps informations about managed torrents.
 */
class TorrentManager
{
	public:
		/** Constructs a TorrentManager object; calls loadTorrent().
		 */
		TorrentManager();

		/** Destroys the object.
		 */
		~TorrentManager();

		/** Converts data in bin to its hexadecimal variant.
		 */
		static String binToHex(const String &data);

		/** Deletes all active clients.
		 */
		void destroy();

		/** Returns a TorrentClient object associated with given info hash. The
		 * method returns 0 if there is no such client.
		 */
		TorrentClient *getClient(const String &infoHash);

		/** Returns client ID.
		 */
		const String &getClientID();

		/** Returns TorrentStruct for given info hash.
		 */
		TorrentStruct *getTorrent(const String &infoHash);

		/** Returns dictionary with torrents.
		 */
		const std::map<String, TorrentStruct *> &getTorrents() {
			return torrents;
		};

		/** Converts data in hex to its binary original.
		 */
		static String hexToBin(const String &data);

		/** Returns true if the torrent has set the private flag; otherwise
		 * returns false.
		 */
		bool isPrivate(const Tairent::Core::BEncode &metaInfo);

		/** Saves informations about managed torrents to resume.xml file in
		 * torrents-directory configuration option.
		 */
		void save();

		/** Returns pointer to the instance of this class.
		 */
		static TorrentManager *self() {
			return torrentManager;
		};

		/** Starts downloading unfinished torrents.
		 */
		void startTorrents();

		/** Stops downloading torrents.
		 */
		void stopTorrents();

	private:
		/** Helper method for converting hex char to an integer.
		 */
		static char hexToBin(char c);

		/** Loads metainfo from a file with given filename. A new
		 * Tairent::Core::BEncode object is created and filled with
		 * informations from the file. Zero is returned if any error occurs
		 * while loading.
		 */
		Tairent::Core::BEncode *loadMetaInfo(const String &filename);

		/** Loads informations about torrents from file resume.xml stored in a
		 * directory whose name is stored in Tairon::Core::Config as
		 * "torrents-directory". This file is created for fast-resuming
		 * torrents.
		 */
		void loadResume();

		/** Loads informations about one torrent from a node.
		 *
		 * \param t Element with informations about torrent.
		 */
		void loadTorrent(TiXmlNode *t);

		/** Loads fast-resume data about one torrent from a node.
		 *
		 * \param n Element with fast-resume data.
		 */
		void loadTorrentResume(TiXmlNode *n);

		/** Loads informations about torrents from file torrents.xml stored in
		 * a directory whose name is stored in Tairon::Core::Config as
		 * "torrents-directory".
		 */
		void loadTorrents();

		/** Creates client ID.
		 */
		void makeClientID();

	private:
		/** Client ID.
		 */
		String clientID;

		/** Timer for calling save() method.
		 */
		Tairon::Net::Timer *saveTimer;

		/** Pointer to the instance of this class.
		 */
		static TorrentManager *torrentManager;

		/** Mapping hash (raw, not human-readable) -> info about torrent.
		 */
		std::map<String, TorrentStruct *> torrents;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
