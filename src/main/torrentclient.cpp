/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/config.h>
#include <tairon/core/log.h>
#include <tairon/net/timer.h>

#include "torrentclient.h"

#include "core/bitfield.h"
#include "connection.h"
#include "storage.h"
#include "torrentmanager.h"
#include "trackerclient.h"
#include "trackermanager.h"

#define max(x, y) (x) < (y) ? (y) : (x)

/** Struct for counting preferred peers when seeding.
 */
struct PreferredPeerStruct
{
	/** Time when the peer was unchoked.
	 */
	int unchokeTime;

	/** Upload rate to the peer.
	 */
	double uploadRate;
};

/** \brief Class for comparing PreferredPeerStructs.
 */
class PreferredPeerLess
{
	public:
		bool operator()(const PreferredPeerStruct &a, const PreferredPeerStruct &b) {
			if (a.unchokeTime < b.unchokeTime)
				return true;
			if (a.unchokeTime == b.unchokeTime)
				return a.uploadRate < b.uploadRate;
			return false;
		};
};

namespace Tairent
{

namespace Main
{

/* {{{ TorrentClient::TorrentClient(TorrentStruct *, const String &, Storage *) */
TorrentClient::TorrentClient(TorrentStruct *t, const String &ih, Storage *s) : downloaded(0), infoHash(ih), recentUnchokesCount(0), storage(s), timerCounter(0), torrent(t), uploaded(0)
{
	if (!s)
		storage = new Storage(getMetaInfo()["info"]);

	if (t->complete)
		storage->complete();

	storage->invalidDataSignal.connect(Tairon::Core::methodFunctor(this, &TorrentClient::invalidDataReceived));
	storage->pieceDownloadedSignal.connect(Tairon::Core::methodFunctor(this, &TorrentClient::pieceDownloadedAndChecked));

	trackerClient = TrackerManager::self()->getTracker(this);
	if (trackerClient)
		trackerClient->getPeers();

	maxConnections = atoi((*Tairon::Core::Config::self())["max-connections"]);
	maxUploads = atoi((*Tairon::Core::Config::self())["max-uploads"]);
	minUploads = atoi((*Tairon::Core::Config::self())["min-uploads"]);

	timer = new Tairon::Net::Timer();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &TorrentClient::rechokeTimeout));
	timer->start(10000);
}
/* }}} */

/* {{{ TorrentClient::~TorrentClient() */
TorrentClient::~TorrentClient()
{
	timer->destroy();

	delete trackerClient;

	// storage deletion is done by torrent manager
}
/* }}} */

/* {{{ TorrentClient::addPeers(const std::list<PeerStruct> &) */
void TorrentClient::addPeers(const std::list<PeerStruct> &peers)
{
	String clientID(TorrentManager::self()->getClientID());

	for (std::list<PeerStruct>::const_iterator it = peers.begin(); it != peers.end(); ++it) {
		if (connections.count(it->id)) // we're already connected to this peer
			continue;
		if (pendingConnections.count(it->id)) // we're trying to connect to this peer
			continue;
		if (it->id == clientID) // don't connect to ourselves
			continue;

		Connection *c = new Connection(it->ip, it->port, it->id, this);
		c->closedSignal.connect(Tairon::Core::methodFunctor(this, &TorrentClient::connectingFailed));
		pendingConnections[it->id] = c;

		if (pendingConnections.size() + connections.size() >= maxConnections)
			return;
	}
}
/* }}} */

/* {{{ TorrentClient::connectingFailed(Connection *) */
void TorrentClient::connectingFailed(Connection *c)
{
	pendingConnections.erase(c->getPeerID());
	delete c;
}
/* }}} */

/* {{{ TorrentClient::connectionClosed(Connection *) */
void TorrentClient::connectionClosed(Connection *c)
{
	storage->unmapPieces(c->getPieceWriter());
	c->destroyPieceWriters();
	storage->unmapPieces(c->getPieceReader());
	c->destroyPieceReaders();

	storage->removeBitField(c->getBitField());

	connections.erase(c->getPeerID());
	delete c;
}
/* }}} */

/* {{{ TorrentClient::getInfoHash() */
const String &TorrentClient::getInfoHash()
{
	return infoHash;
}
/* }}} */

/* {{{ TorrentClient::getMetaInfo() */
const Tairent::Core::BEncode &TorrentClient::getMetaInfo()
{
	return *torrent->metaInfo;
}
/* }}} */

/* {{{ TorrentClient::getRemainingSize() */
uint64_t TorrentClient::getRemainingSize()
{
	return storage->getRemainingSize();
}
/* }}} */

/* {{{ TorrentClient::getStorage() */
Storage *TorrentClient::getStorage()
{
	return storage;
}
/* }}} */

/* {{{ TorrentClient::gotBitField(Connection *) */
void TorrentClient::gotBitField(Connection *c)
{
	storage->addBitField(c->getBitField());
	if (storage->shouldBeInterested(c->getBitField()))
		c->sendInterested();
}
/* }}} */

/* {{{ TorrentClient::gotChoke(Connection *) */
void TorrentClient::gotChoke(Connection *c)
{
	storage->reRequest(c->getRequested());
	c->clearRequested();
}
/* }}} */

/* {{{ TorrentClient::gotHave(uint32_t, Connection *) */
void TorrentClient::gotHave(uint32_t index, Connection *c)
{
	if (storage->gotHave(index)) {
		c->sendInterested();
		if (!c->isChoked() && (c->getRequestsCount() < 5)) // TODO: do it better
			requestMore(c);
	}
}
/* }}} */

/* {{{ TorrentClient::gotInterestedChange(Connection *) */
void TorrentClient::gotInterestedChange(Connection *c)
{
	if (c->isPeerChoked())
		return;
	rechoke();
}
/* }}} */

/* {{{ TorrentClient::gotPiece(uint32_t, uint32_t, Connection *) */
void TorrentClient::gotPiece(uint32_t index, uint32_t start, Connection *c)
{
	downloaded += c->getPieceLength();
	storage->gotPiece(index, start, c);
}
/* }}} */

/* {{{ TorrentClient::gotUnchoke(Connection *) */
void TorrentClient::gotUnchoke(Connection *c)
{
	if (c->isInterested())
		requestMore(c);
}
/* }}} */

/* {{{ TorrentClient::invalidDataReceived(Connection *) */
void TorrentClient::invalidDataReceived(Connection *c)
{
	c->close();
	connectionClosed(c);
}
/* }}} */

/* {{{ TorrentClient::newConnection(Connection *, const String &) */
void TorrentClient::newConnection(Connection *c, const String &oldPeerID)
{
	if (pendingConnections.count(oldPeerID))
		pendingConnections.erase(oldPeerID);

	if (pendingConnections.size() + connections.size() >= maxConnections) {
		c->close();
		delete c;
		return;
	}

	if (connections.count(c->getPeerID())) { // already connected peer
		c->close();
		delete c;
		return;
	}

	connections[c->getPeerID()] = c;
	c->closedSignal.connect(Tairon::Core::methodFunctor(this, &TorrentClient::connectionClosed));
}
/* }}} */

/* {{{ TorrentClient::pieceDownloaded(uint32_t, uint32_t, Connection *) */
void TorrentClient::pieceDownloaded(uint32_t index, uint32_t start, Connection *c)
{
	storage->pieceDownloaded(index, start);
	requestMore(c);
}
/* }}} */

/* {{{ TorrentClient::pieceDownloadedAndChecked(uint32_t) */
void TorrentClient::pieceDownloadedAndChecked(uint32_t index)
{
	std::map<String, Connection *> temp(connections);
	for (std::map<String, Connection *>::const_iterator it = temp.begin(); it != temp.end(); ++it)
		it->second->sendHave(index);
}
/* }}} */

/* {{{ TorrentClient::pieceSent(uint32_t, uint32_t, Connection *) */
void TorrentClient::pieceSent(uint32_t index, uint32_t length, Connection *c)
{
	storage->pieceSent(index);
	uploaded += length;
}
/* }}} */

/* {{{ TorrentClient::rechoke() */
void TorrentClient::rechoke()
{
	DEBUG("rechoking");

	// mapping connections sorted by their upload rate
	std::multimap<double, Connection *> preferred;
	for (std::map<String, Connection *>::iterator it = connections.begin(); it != connections.end(); ++it)
		if (it->second->isPeerInterested() && !it->second->isSnubbed())
			preferred.insert(std::pair<double, Connection *>(it->second->getIncomingRate(), it->second));

	// number of preferred upload slots
	int prefCount = preferred.size();
	if (maxUploads < prefCount)
		prefCount = maxUploads;

	// pick preferred connections
	std::set<Connection *> mask;
	for (std::multimap<double, Connection *>::reverse_iterator it = preferred.rbegin(); it != preferred.rend(); ++it) {
		mask.insert(it->second);
		if (!--prefCount)
			break;
	}

	// number of connections that are not preferred and we want them unchoked
	int count = 1;
	if (minUploads - prefCount > count)
		count = minUploads - prefCount;

	// unchoke preferred connections, remaining just put in a list
	Connection **toUnchoke = new Connection *[connections.size() - mask.size()];
	int toUnchokeIndex = 0;
	std::map<String, Connection *> temp(connections);
	for (std::map<String, Connection *>::iterator it = temp.begin(); it != temp.end(); ++it)
		if (mask.count(it->second))
			it->second->sendUnchoke(timerCounter);
		else
			toUnchoke[toUnchokeIndex++] = it->second;

	// shuffle the list
	for (int i = 0; i < toUnchokeIndex; ++i) {
		int r = random() % toUnchokeIndex;
		Connection *temp = toUnchoke[i];
		toUnchoke[i] = toUnchoke[r];
		toUnchoke[r] = temp;
	}

	// unchoke remaining connections and choke the rest
	for (int i = 0; i < toUnchokeIndex; ++i)
		if (count) {
			if (toUnchoke[i]->isPeerInterested())
				--count;
			toUnchoke[i]->sendUnchoke(timerCounter);
		} else
			toUnchoke[i]->sendChoke();

	delete [] toUnchoke;
}
/* }}} */

/* {{{ TorrentClient::rechokeSeeding(bool) */
void TorrentClient::rechokeSeeding(bool forceNewUnchokes)
{
	// number of forced unchokes
	int forceUnchokesCount = 0;

	if (forceNewUnchokes) {
		int i = (maxUploads + 2) / 3;
		forceUnchokesCount = max(0, (i + timerCounter % 3) / 3 - recentUnchokesCount);
	}

	int timeLimit = timerCounter - 3;

	// sorted list of connections: first come connections that have been
	// recently unchoked, then connections with higher upload rate
	std::multimap<PreferredPeerStruct, Connection *, PreferredPeerLess> preferred;
	for (std::map<String, Connection *>::iterator it = connections.begin(); it != connections.end(); ++it)
		if (!it->second->isPeerChoked() && it->second->isPeerInterested() && it->second->getBitField()->getRemaining()) {
			PreferredPeerStruct pp;
			pp.uploadRate = -it->second->getOutgoingRate();
			if (it->second->getUnchokeTime() > timeLimit)
				pp.unchokeTime = -it->second->getUnchokeTime();
			else
				pp.unchokeTime = 1;
			preferred.insert(std::pair<PreferredPeerStruct, Connection *>(pp, it->second));
		}

	// number of connections that aren't rechoked
	int numKept = maxUploads - forceUnchokesCount;

	// pick preferred connections
	std::set<Connection *> mask;
	for (std::multimap<PreferredPeerStruct, Connection *, PreferredPeerLess>::iterator it = preferred.begin(); (it != preferred.end()) && numKept; ++it, --numKept)
		mask.insert(it->second);

	// number of connections that are not preferred and we want them unchoked
	int count = maxUploads - mask.size();

	recentUnchokesCount = forceNewUnchokes ? 0 : recentUnchokesCount + count;

	// create list of connections to unchoke
	Connection **toUnchoke = new Connection *[connections.size() - mask.size()];
	int toUnchokeIndex = 0;
	for (std::map<String, Connection *>::iterator it = connections.begin(); it != connections.end(); ++it)
		if (!mask.count(it->second))
			toUnchoke[toUnchokeIndex++] = it->second;

	// shuffle the list
	for (int i = 0; i < toUnchokeIndex; ++i) {
		int r = random() % toUnchokeIndex;
		Connection *temp = toUnchoke[i];
		toUnchoke[i] = toUnchoke[r];
		toUnchoke[r] = temp;
	}

	// unchoke remaining connections and choke the rest
	for (int i = 0; i < toUnchokeIndex; ++i) {
		Connection *c = toUnchoke[i];
		if (!c->isPeerInterested())
			c->sendChoke();
		else if (c->isPeerChoked()) {
			if (count > 0) {
				c->sendUnchoke(timerCounter);
				count -= 1;
			}
		} else
			if (!count)
				c->sendChoke();
			else
				count -= 1;
	}

	delete [] toUnchoke;
}
/* }}} */

/* {{{ TorrentClient::rechokeTimeout() */
void TorrentClient::rechokeTimeout()
{
	timerCounter += 1;
	if (!storage->getBitField()->getRemaining())
		rechokeSeeding();
	else
		rechoke();
}
/* }}} */

/* {{{ TorrentClient::requestMore(Connection *) */
void TorrentClient::requestMore(Connection *c)
{
	uint32_t index;
	uint32_t start;
	uint32_t length;

	if (storage->pickPiece(c->getBitField(), index, start, length))
		c->sendRequest(index, start, length);
	else
		c->sendNotInterested();
}
/* }}} */

/* {{{ TorrentClient::stop() */
void TorrentClient::stop()
{
	if (trackerClient)
		trackerClient->stop();

	for (std::map<String, Connection *>::const_iterator it = connections.begin(); it != connections.end(); ++it) {
		Connection *c = it->second;
		c->close();
		storage->unmapPieces(c->getPieceWriter());
		c->destroyPieceWriters();
		storage->unmapPieces(c->getPieceReader());
		c->destroyPieceReaders();
		delete c;
	}
	connections.clear();

	for (std::map<String, Connection *>::const_iterator it = pendingConnections.begin(); it != pendingConnections.end(); ++it) {
		it->second->close();
		delete it->second;
	}
	pendingConnections.clear();
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
