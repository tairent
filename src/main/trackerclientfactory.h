/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_trackerclientfactory_h
#define _main_trackerclientfactory_h

namespace Tairent
{

namespace Main
{

class TorrentClient;
class TrackerClient;

/** \brief Base class for factories that create tracker clients.
 */
class TrackerClientFactory
{
	public:
		/** Creates a new object.
		 */
		TrackerClientFactory();

		/** Destroys the object.
		 */
		virtual ~TrackerClientFactory();

		/** Creates new TrackerClient object for a client.
		 */
		virtual TrackerClient *createTrackerClient(TorrentClient *client) = 0;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
