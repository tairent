/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/mutex.h>

#include "hashcheckerthread.h"

#include "hashchecker.h"

namespace Tairent
{

namespace Main
{

HashCheckerThread *HashCheckerThread::hashCheckerThread = 0;

/* {{{ HashCheckerThread::HashCheckerThread() */
HashCheckerThread::HashCheckerThread() : Tairon::Core::Thread("hash"), exit(false)
{
	deletingMutex = new Tairon::Core::Mutex();
	hashCheckerThread = this;
}
/* }}} */

/* {{{ HashCheckerThread::~HashCheckerThread() */
HashCheckerThread::~HashCheckerThread()
{
	delete deletingMutex;
	hashCheckerThread = 0;
}
/* }}} */

/* {{{ HashCheckerThread::addCheckerToDelete(HashChecker *) */
void HashCheckerThread::addCheckerToDelete(HashChecker *checker)
{
	deletingMutex->lock();
	toDelete.push_back(checker);
	deletingMutex->unlock();
}
/* }}} */

/* {{{ HashCheckerThread::run() */
void *HashCheckerThread::run()
{
	while (!exit) {
		waitAndCallFunctors(1, 0);
		deletingMutex->lock();
		for (std::list<HashChecker *>::const_iterator it = toDelete.begin(); it != toDelete.end(); ++it)
			delete *it;
		toDelete.clear();
		deletingMutex->unlock();
	}

	return 0;
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
