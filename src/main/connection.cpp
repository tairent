/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <arpa/inet.h>

#include <tairon/core/log.h>
#include <tairon/core/thread.h>
#include <tairon/net/ireader.h>
#include <tairon/net/limiter.h>
#include <tairon/net/socket.h>
#include <tairon/net/swriter.h>
#include <tairon/net/timer.h>

#include "connection.h"

#include "core/bencode.h"
#include "core/bitfield.h"
#include "ratemeasurer.h"
#include "storage.h"
#include "torrentclient.h"
#include "torrentmanager.h"
#include "torrentserver.h"

namespace Tairent
{

namespace Main
{

const unsigned int handshakeLength = 48; // without peer id
const char *protocolName = "BitTorrent protocol";

Tairon::Net::Limiter *rlimiter = 0;
Tairon::Net::Limiter *wlimiter = 0;

/* {{{ Connection::Connection(int) */
Connection::Connection(int fd) : bitfield(0), choked(true), client(0), commandReader(0), deleteReader(true), interested(false), lastPieceTime(0), lengthReader(0), peerChoked(true), peerInterested(false), pieceReaders(0), pieceWriters(0), reader(0)
{
	socket = new Tairon::Net::Socket(fd);
	init();

	incomingRate = new RateMeasurer(10);
	outgoingRate = new RateMeasurer(10);
}
/* }}} */

/* {{{ Connection::Connection(const String &, uint16_t, const String &, TorrentClient *) */
Connection::Connection(const String &ip, uint16_t port, const String &pID, TorrentClient *c) : bitfield(0), choked(true), client(c), commandReader(0), deleteReader(true), interested(false), lastPieceTime(0), lengthReader(0), peerChoked(true), peerID(pID), peerInterested(false), pieceReaders(0), pieceWriters(0), reader(0)
{
	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	socket = new Tairon::Net::Socket(Tairon::Net::Socket::IPv4, Tairon::Net::Socket::Stream);
	socket->connectedSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::socketConnected));
	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::connectingError));

	incomingRate = new RateMeasurer(10);
	outgoingRate = new RateMeasurer(10);

	socket->connect(ip, port);
}
/* }}} */

/* {{{ Connection::~Connection() */
Connection::~Connection()
{
	delete incomingRate;
	delete outgoingRate;
	delete bitfield;
	if (deleteReader)
		delete reader;
	delete commandReader;
	delete lengthReader;

	for (std::list<Tairon::Net::Writer *>::const_iterator it = commandQueue.begin(); it != commandQueue.end(); ++it)
		delete *it;
}
/* }}} */

/* {{{ Connection::readBitField(Tairon::Net::Reader *) */
void Connection::readBitField(Tairon::Net::Reader *)
{
	DEBUG("bitfield read");
	bitfield = new Tairent::Core::BitField(reader->getBuffer(), client->getMetaInfo()["info"]["pieces"].asString().length() / 20);
	delete reader;
	reader = lengthReader;
	deleteReader = false;

	client->gotBitField(this);
}
/* }}} */

/* {{{ Connection::readCancel(Tairon::Net::Reader *) */
void Connection::readCancel(Tairon::Net::Reader *)
{
	uint32_t index = htonl(*(uint32_t *) reader->getBuffer());
	uint32_t start = htonl(*(uint32_t *) (reader->getBuffer() + 4));
	uint32_t length = htonl(*(uint32_t *) (reader->getBuffer() + 8));

	for (std::list<PieceRequestStruct>::iterator it = peersRequests.begin(); it != peersRequests.end(); ++it)
		if ((it->index == index) && (it->start == start) && (it->length == length)) {
			peersRequests.erase(it);
			break;
		}

	delete reader;
	reader = lengthReader;
	deleteReader = false;
}
/* }}} */

/* {{{ Connection::readCommand(Tairon::Net::Reader *) */
void Connection::readCommand(Tairon::Net::Reader *)
{
	commandReader->reset();
	switch (*reader->getBuffer()) {
		case 0:
			if (commandLength != 1) {
				DEBUG("Invalid length of choke command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("choke");
			reader = lengthReader;
			if (choked) // we're already choked
				break;
			choked = true;
			client->gotChoke(this);
			break;
		case 1:
			if (commandLength != 1) {
				DEBUG("Invalid length of unchoke command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("unchoke");
			reader = lengthReader;
			if (!choked) // we're already unchoked
				break;
			choked = false;
			client->gotUnchoke(this);
			break;
		case 2:
			if (commandLength != 1) {
				DEBUG("Invalid length of interested command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("interested");
			reader = lengthReader;
			peerInterested = true;
			client->gotInterestedChange(this);
			break;
		case 3:
			if (commandLength != 1) {
				DEBUG("Invalid length of not interested command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("not interested");
			reader = lengthReader;
			peerInterested = false;
			client->gotInterestedChange(this);
			break;
		case 4:
			if (commandLength != 5) {
				DEBUG("Invalid length of have command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("have");
			reader = new Tairon::Net::IReader(4, socket, rlimiter);
			reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readHave));
			reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
			deleteReader = true;
			break;
		case 5:
			DEBUG("bitfield");
			close();
			closedSignal.emit(this);
			return;
		case 6:
			if (commandLength != 13) {
				DEBUG("Invalid length of request command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("request");
			reader = new Tairon::Net::IReader(12, socket, rlimiter);
			reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readRequest));
			reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
			deleteReader = true;
			break;
		case 7:
			DEBUG("piece");
			reader = new Tairon::Net::IReader(8, socket, rlimiter);
			reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readPiece));
			reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
			lastPieceTime = Tairon::Net::Timer::currentTime();
			deleteReader = true;
			break;
		case 8:
			if (commandLength != 13) {
				DEBUG("Invalid length of cancel command");
				close();
				closedSignal.emit(this);
				return;
			}
			DEBUG("cancel");
			reader = new Tairon::Net::IReader(12, socket, rlimiter);
			reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readCancel));
			reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
			deleteReader = true;
			break;
		default:
			DEBUG((const char *) String("unknown message" + String::number(*reader->getBuffer())));
			close();
			closedSignal.emit(this);
	}
}
/* }}} */

/* {{{ Connection::readFirstCommand(Tairon::Net::Reader *) */
void Connection::readFirstCommand(Tairon::Net::Reader *)
{
	commandReader->bufferFullSignal.clear();
	commandReader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readCommand));
	commandReader->reset();

	if (*reader->getBuffer() == 5) { // bitfield
		DEBUG("bitfield");

		// check if the peer isn't sending fake data
		uint32_t len = client->getMetaInfo()["info"]["pieces"].asString().length() / 20;
		if ((commandLength - 1) != (len / 8 + (len % 8 ? 1 : 0))) {
			ERROR("Peer sent an invalid bitfield length");
			socket->close();
			closedSignal.emit(this);
			return;
		}

		reader = new Tairon::Net::IReader(commandLength - 1, socket, rlimiter);
		reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readBitField));
		reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));

		deleteReader = true;
	} else {
		bitfield = new Tairent::Core::BitField(client->getMetaInfo()["info"]["pieces"].asString().length() / 20);
		readCommand(reader);
	}
}
/* }}} */

/* {{{ Connection::readHandshake(Tairon::Net::Reader *) */
void Connection::readHandshake(Tairon::Net::Reader *)
{
	DEBUG("got handshake 1");

	const char *buffer = reader->getBuffer();

	// check the protocol length (in the case that reader emitted
	// bufferFullSignal instead of dataReadSignal.
	if (*buffer != 19) {
		WARNING("Invalid protocol length");
		close();
		closedSignal.emit(this);
		return;
	}

	if (strstr(buffer + 1, protocolName) != (buffer + 1)) { // invalid protocol name
		WARNING("Invalid protocol name");
		close();
		closedSignal.emit(this);
		return;
	}

	DEBUG("Protocol ok");

	if (!client) {
		String infoHash(buffer + 28, 20);
		DEBUG("sending handshake");
		client = TorrentManager::self()->getClient(infoHash);
		if (!client) {
			WARNING("Got unknown infohash");
			close();
			closedSignal.emit(this);
			return;
		}
		sendHandshake(infoHash);
	}

	delete reader;

	reader = new Tairon::Net::IReader(20, socket, rlimiter);
	reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readPeerID));
	reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
}
/* }}} */

/* {{{ Connection::readHave(Tairon::Net::Reader *) */
void Connection::readHave(Tairon::Net::Reader *)
{
	uint32_t index = htonl(*(uint32_t *) reader->getBuffer());

	if (index > bitfield->getLength()) {
		WARNING("Peer sent invalid have index");
		close();
		closedSignal.emit(this);
		return;
	}

	delete reader;
	reader = lengthReader;
	deleteReader = false;

	bitfield->setBit(index);
	client->gotHave(index, this);
}
/* }}} */

/* {{{ Connection::readLength(Tairon::Net::Reader *) */
void Connection::readLength(Tairon::Net::Reader *)
{
	commandLength = ntohl(*(uint32_t *) lengthReader->getBuffer());
	DEBUG((const char *) String("Got message of length " + String::number(commandLength)));

	lengthReader->reset();

	if (commandLength)
		reader = commandReader;
	// else keepalive
}
/* }}} */

/* {{{ Connection::readPeerID(Tairon::Net::Reader *) */
void Connection::readPeerID(Tairon::Net::Reader *)
{
	DEBUG("got peer id");
	String pID(reader->getBuffer(), 20);

	if ((peerID.length() == 20) && (peerID != pID)) { // we are connecting to a peer and it's ID doesn't match
		WARNING("Peer's ID doesn't match");
		close();
		closedSignal.emit(this);
		return;
	}

	String oldPeerID = peerID;
	peerID = pID;

	delete reader;

	lengthReader = new Tairon::Net::IReader(4, socket, rlimiter);
	lengthReader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readLength));
	lengthReader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));

	commandReader = new Tairon::Net::IReader(1, socket, rlimiter);
	commandReader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readFirstCommand));
	commandReader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));

	reader = lengthReader;
	deleteReader = false;

	TorrentServer::self()->connectionCompleted(this);
	client->newConnection(this, oldPeerID);
}
/* }}} */

/* {{{ Connection::readPiece(Tairon::Net::Reader *) */
void Connection::readPiece(Tairon::Net::Reader *)
{
	pieceIndex = ntohl(*(uint32_t *) reader->getBuffer());
	pieceStart = ntohl(*(uint32_t *) (reader->getBuffer() + 4));
	delete reader;

	std::set<uint32_t> &r = requests[pieceIndex];
	r.erase(pieceStart);
	if (!r.size())
		requests.erase(pieceIndex);

	deleteReader = false;
	client->gotPiece(pieceIndex, pieceStart, this);
}
/* }}} */

/* {{{ Connection::readProtocolLength(Tairon::Net::Reader *, size_t) */
void Connection::readProtocolLength(Tairon::Net::Reader *, size_t)
{
	if (*reader->getBuffer() != 19) { // length of the protocol name
		WARNING("Invalid protocol length");
		close();
		closedSignal.emit(this);
		return;
	}

	reader->dataReadSignal.clear();
}
/* }}} */

/* {{{ Connection::readRequest(Tairon::Net::Reader *) */
void Connection::readRequest(Tairon::Net::Reader *)
{
	if (peerChoked) { // don't send anything to the choked peer
		delete reader;
		reader = lengthReader;
		deleteReader = false;
		return;
	}

	PieceRequestStruct rs;
	rs.index = htonl(*(uint32_t *) reader->getBuffer());
	rs.start = htonl(*(uint32_t *) (reader->getBuffer() + 4));
	rs.length = htonl(*(uint32_t *) (reader->getBuffer() + 8));

	if (rs.length > 16384) {
		WARNING((const char *) String("Client requested too big chunk: " + String::number(rs.length)));
		close();
		closedSignal.emit(this);
		return;
	}

	delete reader;
	reader = lengthReader;
	deleteReader = false;

	if (pieceWriters || commandQueue.size()) // we are sending something
		peersRequests.push_back(rs);
	else
		client->getStorage()->gotRequest(rs.index, rs.start, rs.length, this);
}
/* }}} */

/* {{{ Connection::readyRead(Tairon::Net::Socket *) */
void Connection::readyRead(Tairon::Net::Socket *)
{
	try {
		reader->read();
	} catch (const Tairon::Net::SocketException &) { // connection closed or other error
		close();
		closedSignal.emit(this);
	}
}
/* }}} */

/* {{{ Connection::readyWrite(Tairon::Net::Socket *) */
void Connection::readyWrite(Tairon::Net::Socket *)
{
	if (pieceWriters)
		pieceWriters->writers.front()->write();
	else
		commandQueue.front()->write();
}
/* }}} */

/* {{{ Connection::addCommandWriter(Tairon::Net::Writer *) */
void Connection::addCommandWriter(Tairon::Net::Writer *writer)
{
	commandQueue.push_back(writer);
	if ((commandQueue.size() == 1) && !pieceWriters) // don't send anything if we are sending something
		writer->write();
}
/* }}} */

/* {{{ Connection::connectingError(Tairon::Net::Socket *, int) */
void Connection::connectingError(Tairon::Net::Socket *, int)
{
	socket->close();
	client->connectingFailed(this);
}
/* }}} */

/* {{{ Connection::close() */
void Connection::close()
{
	socket->close();
}
/* }}} */

/* {{{ Connection::clearRequested() */
void Connection::clearRequested()
{
	requests.clear();
}
/* }}} */

/* {{{ Connection::dataWritten(Tairon::Net::Writer *) */
void Connection::dataWritten(Tairon::Net::Writer *writer)
{
	DEBUG("dataWritten");

	Tairon::Net::Writer *w = commandQueue.front();
	if (w == writer) // is this writer in the queue?
		commandQueue.pop_front(); // remove it
	delete writer;

	if (commandQueue.size()) // are there some commands to send?
		commandQueue.front()->write();
	else if (peersRequests.size()) { // let's try to send some piece
		PieceRequestStruct rs = peersRequests.front();
		peersRequests.pop_front();
		client->getStorage()->gotRequest(rs.index, rs.start, rs.length, this);
	}
}
/* }}} */

/* {{{ Connection::destroyPieceReaders() */
void Connection::destroyPieceReaders()
{
	if (!pieceReaders)
		return;

	for (std::list<Tairon::Net::Reader *>::iterator it = pieceReaders->readers.begin(); it != pieceReaders->readers.end(); ++it)
		delete *it;
	delete pieceReaders;
	pieceReaders = 0;
}
/* }}} */

/* {{{ Connection::destroyPieceWriters() */
void Connection::destroyPieceWriters()
{
	if (!pieceWriters)
		return;

	for (std::list<Tairon::Net::Writer *>::iterator it = pieceWriters->writers.begin(); it != pieceWriters->writers.end(); ++it)
		delete *it;
	delete pieceWriters;
	pieceWriters = 0;
}
/* }}} */

/* {{{ Connection::getIncomingRate() */
double Connection::getIncomingRate()
{
	return incomingRate->getRate();
}
/* }}} */

/* {{{ Connection::getOutgoingRate() */
double Connection::getOutgoingRate()
{
	return outgoingRate->getRate();
}
/* }}} */

/* {{{ Connection::getPieceLength() */
uint32_t Connection::getPieceLength()
{
	// 1 byte message type, 4 bytes index, 4 bytes start
	return commandLength - 9;
}
/* }}} */

/* {{{ Connection::getReadingLimiter() */
Tairon::Net::Limiter *Connection::getReadingLimiter()
{
	return rlimiter;
}
/* }}} */

/* {{{ Connection::getRequestsCount() */
unsigned int Connection::getRequestsCount()
{
	unsigned int ret = 0;
	for (std::map<uint32_t, std::set<uint32_t> >::const_iterator it = requests.begin(); it != requests.end(); ++it)
		ret += it->second.size();
	return ret;
}
/* }}} */

/* {{{ Connection::getWritingLimiter() */
Tairon::Net::Limiter *Connection::getWritingLimiter()
{
	return wlimiter;
}
/* }}} */

/* {{{ Connection::init() */
void Connection::init()
{
	Tairon::Core::Thread *current = Tairon::Core::Thread::current();

	socket->errorSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::socketError));
	socket->readyReadSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::readyRead));
	socket->readyWriteSignal.connect(Tairon::Core::threadMethodDFunctor(current, this, &Connection::readyWrite));
	socket->ready();

	reader = new Tairon::Net::IReader(handshakeLength, socket, rlimiter);
	reader->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readHandshake));
	// it is safe to do this, we just want to check the first byte
	reader->dataReadSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readProtocolLength));
	reader->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
}
/* }}} */

/* {{{ Connection::isSnubbed() */
bool Connection::isSnubbed()
{
	return Tairon::Net::Timer::currentTime() - lastPieceTime > 10000; // 10 seconds, should be configurable
}
/* }}} */

/* {{{ Connection::pieceDataWritten(Tairon::Net::Writer *) */
void Connection::pieceDataWritten(Tairon::Net::Writer *writer)
{
	pieceWriters->writers.pop_front();

	if (pieceWriters->writers.size()) { // there is still something to send from the piece
		delete writer; // delete the current writer
		pieceWriters->writers.front()->write();
	} else { // whole piece has been sent
		outgoingRate->update(pieceWriters->length);
		client->pieceSent(pieceWriters->index, pieceWriters->length, this);
		// the last piece's writer will be deleted in dataWritten() method
		delete pieceWriters;
		pieceWriters = 0;

		dataWritten(writer);
	}
}
/* }}} */

/* {{{ Connection::readerError(Tairon::Net::Reader *, Tairon::Net::Socket *) */
void Connection::readerError(Tairon::Net::Reader *, Tairon::Net::Socket *)
{
	closedSignal.emit(this);
}
/* }}} */

/* {{{ Connection::sendChoke() */
void Connection::sendChoke()
{
	DEBUG("sending choke");

	if (peerChoked)
		return;

	peersRequests.clear();

	peerChoked = true;
	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String("\x00\x00\x00\x01\x00", 5), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::sendHandshake(const String &) */
void Connection::sendHandshake(const String &infoHash)
{
	char handshake[68];
	handshake[0] = (char) 19;
	memcpy(handshake + 1, protocolName, 19);
	memset(handshake + 20, 0, 8);
	memcpy(handshake + 28, infoHash.data(), 20);
	memcpy(handshake + 48, TorrentManager::self()->getClientID().data(), 20);

	String h(handshake, 68);

	Tairon::Net::Writer *writer = new Tairon::Net::SWriter(h, socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);

	if (client->getStorage()->hasSomething()) { // we have already downloaded some piece, send bitfield as well
		DEBUG("sending bitfield");
		// count length of the bitfield
		size_t length = client->getStorage()->getBitField()->getLength();
		length = length / 8 + (length % 8 ? 1 : 0);

		char *bf = new char[5 + length];

		uint32_t l = htonl(1 + length); // 1 byte message type, rest is bitfield
		memcpy(bf, (const char *) (&l), 4);
		bf[4] = 5; // bitfield message
		memcpy(bf + 5, client->getStorage()->getBitField()->getData(), length);

		String b(bf, 5 + length);
		writer = new Tairon::Net::SWriter(b, socket, wlimiter);
		writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
		writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

		addCommandWriter(writer);

		delete [] bf;
	}
}
/* }}} */

/* {{{ Connection::sendHave(uint32_t) */
void Connection::sendHave(uint32_t index)
{
	DEBUG("sending have");

	uint32_t i = htonl(index);
	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String("\x00\x00\x00\x05\x04", 5) + String((const char *) (&i), 4), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::sendInterested() */
void Connection::sendInterested()
{
	DEBUG("sending interested");

	if (interested)
		return;

	interested = true;
	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String("\x00\x00\x00\x01\x02", 5), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::sendNotInterested() */
void Connection::sendNotInterested()
{
	DEBUG("sending not interested");

	if (!interested)
		return;

	interested = false;
	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String("\x00\x00\x00\x01\x03", 5), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::sendRequest(uint32_t, uint32_t, uint32_t) */
void Connection::sendRequest(uint32_t index, uint32_t start, uint32_t length)
{
	requests[index].insert(start);

	char msg[17];
	memcpy(msg, "\x00\x00\x00\x0d\x06", 5);
	*(uint32_t *) (msg + 5) = htonl(index);
	*(uint32_t *) (msg + 9) = htonl(start);
	*(uint32_t *) (msg + 13) = htonl(length);

	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String(msg, 17), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::sendUnchoke(int) */
void Connection::sendUnchoke(int time)
{
	DEBUG("sending unchoke");

	if (!peerChoked)
		return;

	unchokeTime = time;

	peerChoked = false;
	Tairon::Net::SWriter *writer = new Tairon::Net::SWriter(String("\x00\x00\x00\x01\x01", 5), socket, wlimiter);
	writer->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::dataWritten));
	writer->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));

	addCommandWriter(writer);
}
/* }}} */

/* {{{ Connection::setNextReader(Tairon::Net::Reader *) */
void Connection::setNextReader(Tairon::Net::Reader *)
{
	delete pieceReaders->readers.front();
	pieceReaders->readers.pop_front();

	if (pieceReaders->readers.size()) {
		DEBUG("Setting next piece reader");
		reader = pieceReaders->readers.front();
	} else {
		DEBUG("Piece downloaded");

		incomingRate->update(pieceReaders->length);

		bool fake = pieceReaders->fake;
		delete pieceReaders;
		pieceReaders = 0;
		reader = lengthReader;
		deleteReader = false;
		if (!fake)
			client->pieceDownloaded(pieceIndex, pieceStart, this);
	}
}
/* }}} */

/* {{{ Connection::setReaders(PieceReadersStruct *) */
void Connection::setReaders(PieceReadersStruct *readers)
{
	for (std::list<Tairon::Net::Reader *>::iterator it = readers->readers.begin(); it != readers->readers.end(); ++it) {
		(*it)->bufferFullSignal.connect(Tairon::Core::methodFunctor(this, &Connection::setNextReader));
		(*it)->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::readerError));
	}
	pieceReaders = readers;
	reader = pieceReaders->readers.front();
}
/* }}} */

/* {{{ Connection::setWriters(PieceWritersStruct *) */
void Connection::setWriters(PieceWritersStruct *writers)
{
	for (std::list<Tairon::Net::Writer *>::iterator it = writers->writers.begin(); it != writers->writers.end(); ++it) {
		(*it)->dataWrittenSignal.connect(Tairon::Core::methodFunctor(this, &Connection::pieceDataWritten));
		(*it)->errorSignal.connect(Tairon::Core::methodFunctor(this, &Connection::writerError));
	}

	pieceWriters = writers;
	writers->writers.front()->write();
}
/* }}} */

/* {{{ Connection::socketConnected(Tairon::Net::Socket *) */
void Connection::socketConnected(Tairon::Net::Socket *)
{
	socket->connectedSignal.clear();
	socket->errorSignal.clear();

	init();

	sendHandshake(client->getInfoHash());
}
/* }}} */

/* {{{ Connection::socketError(Tairon::Net::Socket *, int) */
void Connection::socketError(Tairon::Net::Socket *, int)
{
	socket->close();
	closedSignal.emit(this);
}
/* }}} */

/* {{{ Connection::writerError(Tairon::Net::Writer *, Tairon::Net::Socket *) */
void Connection::writerError(Tairon::Net::Writer *, Tairon::Net::Socket *)
{
	closedSignal.emit(this);
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
