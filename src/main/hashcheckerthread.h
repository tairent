/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_hashcheckerthread_h
#define _main_hashcheckerthread_h

#include <list>

#include <tairon/core/thread.h>

namespace Tairon
{

namespace Core
{

class Mutex;

}; // namespace Core

}; // namespace Tairon

namespace Tairent
{

namespace Main
{

class HashChecker;

/** \brief Thread for hash checking.
 */
class HashCheckerThread : public Tairon::Core::Thread
{
	public:
		/** Constructs a HashCheckerThread object. It is still needed to call
		 * start() method to run the thread.
		 */
		HashCheckerThread();

		/** Destroys the thread.
		 */
		virtual ~HashCheckerThread();

		/** Adds a checker to the queue of checkers scheduled for deletion.
		 */
		void addCheckerToDelete(HashChecker *checker);

		/** Main thread's loop. Just delivers signals to HashChecker objects.
		 */
		virtual void *run();

		/** Returns pointer to the instance of this class.
		 */
		static HashCheckerThread *self() {
			return hashCheckerThread;
		};

		/** Stops the thread's loop.
		 */
		void stop() {
			exit = true;
		};

	private:
		/** Mutex for locking toDelete list.
		 */
		Tairon::Core::Mutex *deletingMutex;

		/** Whether this thread should exit.
		 */
		bool exit;

		/** Pointer to the instance of this class.
		 */
		static HashCheckerThread *hashCheckerThread;

		/** List of checkers that should be deleted.
		 */
		std::list<HashChecker *> toDelete;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
