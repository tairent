/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/net/timer.h>

#include "ratemeasurer.h"

namespace Tairent
{

namespace Main
{

/* {{{ RateMeasurer::RateMeasurer(double) */
RateMeasurer::RateMeasurer(double p) : period(p)
{
	lastTime = (double) Tairon::Net::Timer::currentTime() / 1000;
	startTime = lastTime - 1;
	rate = 0;
}
/* }}} */

/* {{{ RateMeasurer::~RateMeasurer() */
RateMeasurer::~RateMeasurer()
{
}
/* }}} */

/* {{{ RateMeasurer::getRate() */
double RateMeasurer::getRate()
{
	double t = (double) Tairon::Net::Timer::currentTime() / 1000;
	if (lastTime + (period / 2) < t)
		update(0);
	return rate;
}
/* }}} */

/* {{{ RateMeasurer::update(unsigned int) */
void RateMeasurer::update(unsigned int amount)
{
	double t = (double) Tairon::Net::Timer::currentTime() / 1000;
	rate = ((rate * (lastTime - startTime)) + amount) / (t - startTime);
	lastTime = t;
	if (startTime < t - period)
		startTime = t - period;
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
