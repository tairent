/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/signals.h>
#include <tairon/core/threadmanager.h>
#include <tairon/crypto/sha1.h>

#include "hashchecker.h"

#include "hashcheckerthread.h"
#include "storage.h"

namespace Tairent
{

namespace Main
{

/* {{{ HashChecker::HashChecker(uint32_t, Storage *) */
HashChecker::HashChecker(uint32_t i, Storage *s) : index(i)
{
	piece = s->getPiece(i);

	pieceHash = s->getMetaInfo()["pieces"].asString().substr(i * 20, 20);

	disabler = new Tairon::Core::FunctorDisabler();
	disabler->incRef();
}
/* }}} */

/* {{{ HashChecker::~HashChecker() */
HashChecker::~HashChecker()
{
	disabler->decRef();
}
/* }}} */

/* {{{ HashChecker::check() */
void HashChecker::check()
{
	Tairon::Core::ThreadFunctorCaller *caller = new Tairon::Core::ThreadMethodDFunctorCaller0<HashChecker>(this, &HashChecker::checkPiece, disabler);
	Tairon::Core::ThreadManager::self()->getThreadByName("hash")->addFunctorCaller(caller);
}
/* }}} */

/* {{{ HashChecker::checkPiece() */
void HashChecker::checkPiece()
{
	Tairon::Crypto::SHA1 hash;

	for (std::list<PieceStruct::FilePieceStruct>::const_iterator it = piece->pieces.begin(); it != piece->pieces.end(); ++it)
		hash.update(it->mem, it->length);

	if (hash.final() == pieceHash)
		hashCorrectSignal.emit(index, this);
	else
		hashIncorrectSignal.emit(index, this);
}
/* }}} */

/* {{{ HashChecker::destroy() */
void HashChecker::destroy()
{
	HashCheckerThread::self()->addCheckerToDelete(this);
}
/* }}} */

}; // namespace Main

}; // namespace Tairent

// vim: ai sw=4 ts=4 noet fdm=marker
