/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2006-2007 David Brodsky                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _main_torrentclient_h
#define _main_torrentclient_h

#include <list>
#include <map>

#include <tairon/core/string.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class Timer;

};

};

namespace Tairent
{

namespace Core
{

class BEncode;

};

namespace Main
{

struct TorrentStruct;

class Connection;
class Storage;
class TrackerClient;

/** Struct with informations about one peer that this client can connect to.
 */
struct PeerStruct
{
	/** Peer's id.
	 */
	String id;

	/** IP address of the peer.
	 */
	String ip;

	/** Port on which the peer is listening.
	 */
	uint16_t port;
};

/** \brief TorrentClient manages running of one torrent.
 */
class TorrentClient
{
	public:
		/** Creates a TorrentClient object.
		 *
		 * \param t Structure that contains informations about this torrent.
		 * \param ih 20 byte SHA1 hash of the info field of metainfo.
		 * \param s Storage created with fast-resume data.
		 */
		TorrentClient(TorrentStruct *t, const String &ih, Storage *s = 0);

		/** Destroys the object.
		 */
		~TorrentClient();

		/** Informs this client about new possible peers.
		 */
		void addPeers(const std::list<PeerStruct> &peers);

		/** Called when a connecting to a peer failed. Erases connection from
		 * the list pending connections and deletes that connection.
		 */
		void connectingFailed(Connection *c);

		/** Called when a connection has been closed.
		 */
		void connectionClosed(Connection *c);

		/** Returns number of bytes downloaded so far.
		 */
		uint64_t getDownloadedSize() {
			return downloaded;
		};

		/** Returns 20 byte SHA1 hash of the info field of metainfo.
		 */
		const String &getInfoHash();

		/** Returns metainfo associated with this client.
		 */
		const Tairent::Core::BEncode &getMetaInfo();

		/** Returns number of bytes that are still left to download.
		 */
		uint64_t getRemainingSize();

		/** Returns client's Storage object.
		 */
		Storage *getStorage();

		/** Returns number of bytes uploaded so far.
		 */
		uint64_t getUploadedSize() {
			return uploaded;
		};

		/** Called when we get a peer bitfield.
		 */
		void gotBitField(Connection *c);

		/** Called when we get a choke.
		 */
		void gotChoke(Connection *c);

		/** Called when we get a message that a peer has something new.
		 *
		 * \param index Index of the piece the peer has.
		 * \param c Connection with the peer.
		 */
		void gotHave(uint32_t index, Connection *c);

		/** Called when the peer changes its interested state.
		 */
		void gotInterestedChange(Connection *c);

		/** Called when we get a piece.
		 *
		 * \param index Index of the piece.
		 * \param start Start of the chunk within the piece.
		 * \param c Connection that got this message.
		 */
		void gotPiece(uint32_t index, uint32_t start, Connection *c);

		/** Called when we get an unchoke.
		 */
		void gotUnchoke(Connection *c);

		/** Called when a new peer is completely connected.
		 *
		 * \param c Connection object.
		 * \param oldPeerID Connection's peer id that was passed to the
		 * constructor.
		 */
		void newConnection(Connection *c, const String &oldPeerID);

		/** Called when a complete piece is downloaded.
		 *
		 * \param index Index of the piece.
		 * \param start Start of the chunk within the piece.
		 * \param c Connection that got this message.
		 */
		void pieceDownloaded(uint32_t index, uint32_t start, Connection *c);

		/** Called when a piece has been sent to the peer.
		 *
		 * \param index Index of the piece.
		 * \param length Length of the piece.
		 * \param c Connection with the peer.
		 */
		void pieceSent(uint32_t index, uint32_t length, Connection *c);

		/** Stops this client.
		 */
		void stop();

	private:
		/** This is connected to Storage::invalidDataSignal and closes the connection.
		 */
		void invalidDataReceived(Connection *c);

		/** Chokes/unchokes peers.
		 */
		void rechoke();

		/** Chokes/unchokes peers when we are seeding.
		 *
		 * \param forceNewUnchokes If set to true then some choked peers will
		 * be unchoked; otherwise no peer may be unchoked.
		 */
		void rechokeSeeding(bool forceNewUnchokes = false);

		/** Called by timer when it's time to rechoke. Calls rechoke() or
		 * rechokeSeeding().
		 */
		void rechokeTimeout();

		/** Requests more pieces from the peer.
		 */
		void requestMore(Connection *c);

		/** Informs this client that a piece has been downloaded and its hash
		 * is correct.
		 */
		void pieceDownloadedAndChecked(uint32_t index);

	private:
		/** Mapping peer id => connection object. These connections are fully
		 * established.
		 */
		std::map<String, Connection *> connections;

		/** Number of bytes downloaded so far.
		 */
		uint64_t downloaded;

		/** 20 byte SHA1 hash of the info field of metainfo.
		 */
		const String &infoHash;

		/** Maximum number of connections this client will handle.
		 */
		unsigned int maxConnections;

		/** Maximum number of uploading slots.
		 */
		int maxUploads;

		/** Minimum number of uploading slots.
		 */
		int minUploads;

		/** Mapping peer id => connection object. These connections are not
		 * fully established.
		 */
		std::map<String, Connection *> pendingConnections;

		/** Number of unchokes since last forced unchoking.
		 */
		int recentUnchokesCount;

		/** Storage for this torrent.
		 */
		Storage *storage;

		/** Timer used for rechoking.
		 */
		Tairon::Net::Timer *timer;

		/** Counts number of timer ticks.
		 */
		int timerCounter;

		/** Informations about this torrent.
		 */
		TorrentStruct *torrent;

		/** Tracker used for this torrent.
		 */
		TrackerClient *trackerClient;

		/** Number of bytes uploaded so far.
		 */
		uint64_t uploaded;
};

}; // namespace Main

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
