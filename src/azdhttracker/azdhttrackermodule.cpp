/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#include <tairon/core/config.h>
#include <tairon/net/timer.h>

#include "azdhttrackermodule.h"

#include "azdht/azdhtmodule.h"
#include "core/bencode.h"
#include "main/torrentclient.h"
#include "main/torrentmanager.h"

namespace Tairent
{

namespace AzDHTTracker
{

/* {{{ AzDHTTrackerModule::AzDHTTrackerModule() */
AzDHTTrackerModule::AzDHTTrackerModule() : Tairon::Core::Module()
{
	timer = new Tairon::Net::Timer();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &AzDHTTrackerModule::init));
	timer->start(10 * 1000, true); // request peers in 10 seconds
}
/* }}} */

/* {{{ AzDHTTrackerModule::~AzDHTTrackerModule() */
AzDHTTrackerModule::~AzDHTTrackerModule()
{
	forAllTorrents(&AzDHTTrackerModule::removeTorrent);
	timer->destroy();
}
/* }}} */

/* {{{ AzDHTTrackerModule::forAllTorrents(void (AzDHTTrackerModule::*)(const String &, Tairent::Main::TorrentStruct *)) */
void AzDHTTrackerModule::forAllTorrents(void (AzDHTTrackerModule::*method)(const String &, Tairent::Main::TorrentStruct *))
{
	if (!Tairent::AzDHT::AzDHTModule::self()->isConnected()) // don't bother if we aren't connected to the DHT
		return;

	Tairent::Main::TorrentManager *manager = Tairent::Main::TorrentManager::self();
	const std::map<String, Tairent::Main::TorrentStruct *> &torrents = manager->getTorrents();
	for (std::map<String, Tairent::Main::TorrentStruct *>::const_iterator it = torrents.begin(); it != torrents.end(); ++it) {
		if (manager->isPrivate(*it->second->metaInfo))
			continue;

		(this->*method)(it->first, it->second);
	}
}
/* }}} */

/* {{{ AzDHTTrackerModule::getPeers(const String &, Tairent::Main::TorrentStruct *) */
void AzDHTTrackerModule::getPeers(const String &infoHash, Tairent::Main::TorrentStruct *torrent)
{
	Tairent::AzDHT::AzDHTModule *module = Tairent::AzDHT::AzDHTModule::self();
	module->registerHandler(infoHash, Tairon::Core::methodFunctor(this, &AzDHTTrackerModule::valuesRead));
	module->get(infoHash, torrent->complete ? Tairent::AzDHT::AzDHTModule::SEEDING : Tairent::AzDHT::AzDHTModule::DOWNLOADING);
}
/* }}} */

/* {{{ AzDHTTrackerModule::init() */
void AzDHTTrackerModule::init()
{
	if (!Tairent::AzDHT::AzDHTModule::self()->isConnected()) {
		timer->start(60 * 1000, true); // try it again in 1 minute
		return;
	}

	timer->timeoutSignal.clear();
	timer->timeoutSignal.connect(Tairon::Core::threadMethodDFunctor(Tairon::Core::Thread::current(), this, &AzDHTTrackerModule::timeout));
	timer->start(10 * 60 * 1000, false); // request peers every 10 minutes

	forAllTorrents(&AzDHTTrackerModule::registerTorrent);

	timeout();
}
/* }}} */

/* {{{ AzDHTTrackerModule::registerTorrent(const String &, Tairent::Main::TorrentStruct *) */
void AzDHTTrackerModule::registerTorrent(const String &infoHash, Tairent::Main::TorrentStruct *torrent)
{
	// Key is torrent's info hash. Value can have many parts separated by
	// semicolon (;) - its first part is address and port for tcp incoming
	// connections separated with colon (:). The address can be missing thus
	// only tcp port is there. Other parts (that aren't used here) of the value
	// can contain udp port or flags.
	Tairent::AzDHT::AzDHTModule::self()->put(infoHash, (*Tairon::Core::Config::self())["torrent-server-port"], torrent->complete ? Tairent::AzDHT::AzDHTModule::SEEDING : Tairent::AzDHT::AzDHTModule::DOWNLOADING);
}
/* }}} */

/* {{{ AzDHTTrackerModule::removeTorrent(const String &, Tairent::Main::TorrentStruct *) */
void AzDHTTrackerModule::removeTorrent(const String &infoHash, Tairent::Main::TorrentStruct *)
{
	Tairent::AzDHT::AzDHTModule::self()->remove(infoHash);
}
/* }}} */

/* {{{ AzDHTTrackerModule::timeout() */
void AzDHTTrackerModule::timeout()
{
	forAllTorrents(&AzDHTTrackerModule::getPeers);
}
/* }}} */

/* {{{ AzDHTTrackerModule::valuesRead(const String &, const Tairent::Core::BEncode &) */
void AzDHTTrackerModule::valuesRead(const String &key, const Tairent::Core::BEncode &values)
{
	Tairent::Main::TorrentManager *manager = Tairent::Main::TorrentManager::self();
	Tairent::Main::TorrentStruct *torrent = manager->getTorrent(key);
	if (!torrent || !torrent->client) // nothing to do
		return;

	std::list<Tairent::Main::PeerStruct> peers;

	const Tairent::Core::BEncode::List &list = values.asList();
	for (Tairent::Core::BEncode::List::const_iterator it = list.begin(); it != list.end(); ++it) {
		const Tairent::Core::BEncode &bvalue = *it;

		// don't connect to seeders if we are one of them
		if (torrent->complete && (bvalue["flags"].asValue() & Tairent::AzDHT::AzDHTModule::SEEDING))
			continue;

		const String &value = bvalue["value"].asString();

		if (value.find('C') != String::npos) // crypted protocol, we don't support it
			continue;

		String tcpPart = value.substr(0, value.find(':'));
		tcpPart.strip();

		String sPort;
		String ip;

		size_t comma = tcpPart.find(':');
		if (comma == String::npos)
			sPort = tcpPart;
		else {
			sPort = tcpPart.substr(comma + 1);
			ip = tcpPart.substr(0, comma);
		}

		int port = atoi(sPort);
		if ((port <= 0) || (port > 65535)) // invalid port
			continue;

		if (!ip.length())
			ip = bvalue["originatorIP"].asString();

		Tairent::Main::PeerStruct ps = {
			ip + ":" + String::number(port), // id
			ip,
			(uint16_t) port
		};
		peers.push_back(ps);
	}

	if (peers.size())
		torrent->client->addPeers(peers);
}
/* }}} */

}; // namespace AzDHTTracker

}; // namespace Tairent

EXPORT_MODULE(azdhttracker, Tairent::AzDHTTracker::AzDHTTrackerModule)

// vim: ai sw=4 ts=4 noet fdm=marker
