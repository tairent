/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2007 David Brodsky                                      *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation and appearing               *
 *   in the file LICENSE.GPL included in the packaging of this file.       *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      *
 *   General Public License for more details.                              *
 *                                                                         *
 ***************************************************************************/

#ifndef _azdhttracker_azdhttrackermodule_h
#define _azdhttracker_azdhttrackermodule_h

#include <tairon/core/module.h>

using Tairon::Core::String;

namespace Tairon
{

namespace Net
{

class Timer;

}; // namespace Net

}; // namespace Tairon

namespace Tairent
{

namespace Core
{

class BEncode;

}; // namespace Core

namespace Main
{

struct TorrentStruct;

}; // namespace Main

namespace AzDHTTracker
{

/** \brief Implementation of DHT tracker.
 */
class AzDHTTrackerModule : public Tairon::Core::Module
{
	public:
		/** Constructs an AzDHTTrackerModule object.
		 */
		AzDHTTrackerModule();

		/** Destroys the object.
		 */
		virtual ~AzDHTTrackerModule();

	private:
		/** Calls given method for all served torrents.
		 */
		void forAllTorrents(void (AzDHTTrackerModule::*method)(const String &, Tairent::Main::TorrentStruct *));

		/** Requests list of peers from the DHT.
		 */
		void getPeers(const String &infoHash, Tairent::Main::TorrentStruct *torrent);

		/** Registers our torrents in the DHT and requests peers.
		 */
		void init();

		/** Registers torrent in the DHT.
		 */
		void registerTorrent(const String &infoHash, Tairent::Main::TorrentStruct *torrent);

		/** Removes torrent's announce from the DHT.
		 */
		void removeTorrent(const String &infoHash, Tairent::Main::TorrentStruct *torrent);

		/** Requests list of peers for all served torrents.
		 */
		void timeout();

		/** Processes list of values supplied by the DHT.
		 *
		 * \param key The key for which the values were requested.
		 * \param values Usually list of values.
		 */
		void valuesRead(const String &key, const Tairent::Core::BEncode &values);

	private:
		/** Timer for getting peers in regular intervals.
		 */
		Tairon::Net::Timer *timer;
};

}; // namespace AzDHTTracker

}; // namespace Tairent

#endif

// vim: ai sw=4 ts=4 noet fdm=marker
