#!/bin/sh

find -regex '.*~' -exec rm -f \{\} \;
rm -rf doc/html
jam clean
